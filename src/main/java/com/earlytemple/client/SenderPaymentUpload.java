/*
 * Copyright 2014 Early Temple
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.earlytemple.client;

import net.minidev.json.JSONObject;

/**
 * This is a class that is generated on the client for upload to the server, after a sender has initiated a contract.
 * It contains the information required for continuation by the server.  The sender copies this as a block of JSON
 * from the client, and paste it into the contract web page.
 **/
public class SenderPaymentUpload extends JSONObject
{
    static final String ET_SERVICE_FEE_TXID = "et_service_fee_txid";
    static final String CONTRACT_LOAD_TXID = "contract_load_txid";
    static final String SUCCESS_TX = "success_tx";
    static final String FAILURE_TX = "failure_tx";
    static final String SENDER_PUBLIC_KEY = "sender_public_key";

    public SenderPaymentUpload()
    {
    }

    public SenderPaymentUpload(JSONObject valid_raw_json)
    {
        merge(valid_raw_json);

        if ( ! containsKey(ET_SERVICE_FEE_TXID) ||
                ! (get(ET_SERVICE_FEE_TXID) instanceof String))
            throw new IllegalArgumentException("Sorry, the code must contain a et_service_fee_txid, which must be a " +
                    "bitcoin transaction ID.");

        if ( ! containsKey(CONTRACT_LOAD_TXID) ||
                ! (get(CONTRACT_LOAD_TXID) instanceof String))
            throw new IllegalArgumentException("Sorry, the code must contain a contract_load_txid, which must be a " +
                    "bitcoin transaction ID.");

        if ( ! containsKey(SUCCESS_TX) ||
                ! (get(SUCCESS_TX) instanceof String))
            throw new IllegalArgumentException("Sorry, the code must contain a success_tx, which must be a " +
                    "bitcoin transaction ID.");

        if ( ! containsKey(FAILURE_TX) ||
                ! (get(FAILURE_TX) instanceof String))
            throw new IllegalArgumentException("Sorry, the code must contain a failure_tx, which must be a " +
                    "bitcoin transaction ID.");
    }

    public void setEtServiceFeeTxid(String et_service_fee_txid)
    {
        put(ET_SERVICE_FEE_TXID, et_service_fee_txid);
    }

    public String getEtServiceFeeTxid()
    {
        return (String) get(ET_SERVICE_FEE_TXID);
    }

    public void setContractLoadTxid(String contract_load_txid)
    {
        put(CONTRACT_LOAD_TXID, contract_load_txid);
    }

    public String getContractLoadTxid()
    {
        return (String) get(CONTRACT_LOAD_TXID);
    }

    public void setSuccessTx(String success_transaction)
    {
        put(SUCCESS_TX, success_transaction);
    }

    public String getSuccessTx()
    {
        return (String) get(SUCCESS_TX);
    }

    public void setFailureTx(String failure_transaction)
    {
        put(FAILURE_TX, failure_transaction);
    }

    public String getFailureTx()
    {
        return (String) get(FAILURE_TX);
    }

    public String getSenderPublicKey()
    {
        return (String) get(SENDER_PUBLIC_KEY);
    }

    public void setSenderPublicKey(String sender_public_key)
    {
        put(SENDER_PUBLIC_KEY, sender_public_key);
    }
}
