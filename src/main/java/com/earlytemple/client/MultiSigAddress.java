/*
 * Copyright 2014 Early Temple
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.earlytemple.client;

/**
 *
 **/
public class MultiSigAddress
{
    private String address;

    private String redeemScript;

    public MultiSigAddress(String address, String redeemScript)
    {
        this.address = address;
        this.redeemScript = redeemScript;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getRedeemScript()
    {
        return redeemScript;
    }

    public void setRedeemScript(String redeemScript)
    {
        this.redeemScript = redeemScript;
    }

    public String toString()
    {
        return "MultiSigAddress{" +
                "address='" + address + '\'' +
                ", redeemScript='" + redeemScript + '\'' +
                '}';
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MultiSigAddress that = (MultiSigAddress) o;

        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (redeemScript != null ? !redeemScript.equals(that.redeemScript) : that.redeemScript != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result = address != null ? address.hashCode() : 0;
        result = 31 * result + (redeemScript != null ? redeemScript.hashCode() : 0);
        return result;
    }
}
