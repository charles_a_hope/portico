/*
 * Copyright 2014 Early Temple
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.earlytemple.client;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.LogManager;


/**
 * Expert system that interacts with the user using the console, and interacts with the bitcoin network using
 * a bitcoin facade.
**/
public class BasicContractExpert
{
    private BitcoinFacade bitcoin_facade;

    public BasicContractExpert(BitcoinFacade bitcoin_facade)
    {
        this.bitcoin_facade = bitcoin_facade;
    }

    public class RecipientData
    {
        public String sender_signed_spend_tx;
        public String sender_signed_refund_tx;
        public String contract_load_txid;
        public String sender_address;

        RecipientData(String sender_signed_spend_tx, String sender_signed_refund_tx, String contract_load_txid,
                      String sender_address)
        {
            this.sender_signed_spend_tx = sender_signed_spend_tx;
            this.sender_signed_refund_tx = sender_signed_refund_tx;
            this.contract_load_txid = contract_load_txid;
            this.sender_address = sender_address;
        }

        public String toString()
        {
            return
                    "Contract transaction ID = " + contract_load_txid + '\n' +
                    "Sender address = " + sender_address + '\n' +
                    "Signed raw redemption transaction = " + sender_signed_spend_tx + '\n' +
                    "Signed raw refund transaction = " + sender_signed_refund_tx + '\n'
            ;
        }
    }

    public static void out(String format, Object ... args)
    {
        System.out.println(String.format(format, args));
    }

    /**
     *
     * @return null if something went wrong and the process should be terminated
     */
    public RecipientData senderRoleInBasicContract(String contract_name, String sender_address,
                                                   String recipient_address, String et_public_key,
                                                   BTC contract_amount, BTC contract_amount_fee)
    {
        BitcoinD bitcoind = bitcoin_facade.getBitcoind();

        /*
         * Extract the user's public key
         */
        String sender_public_key;
        try
        {
            sender_public_key = bitcoind.getPublicKey(sender_address);
            if (sender_public_key == null)
            {
                out("Sorry, I can't find record of your Bitcoin address %s. Perhaps it was entered incorrectly.",
                        sender_address);
                return null;
            }
        }
        catch (BitcoinException e)
        {
            out("Sorry, there was a problem extracting the public-key from your address %s. Your Bitcoin program " +
                    "said %s", sender_address, e.getLocalizedMessage());
            return null;
        }

        /*
         * Create a 2/2 multi-signature address for the contract.  There is no need to save it to the address book.
         */
        String contract_address;
        String redeemScript;
        try
        {
            MultiSigAddress multiSigAddress = bitcoind.createMultiSigAddress(2, sender_public_key, et_public_key);
            contract_address = multiSigAddress.getAddress();
            redeemScript = multiSigAddress.getRedeemScript();
            out("Created 2/2 multi-signature contract address %s", contract_address);
        }
        catch (BitcoinException e)
        {
            out("Sorry, there was a problem creating a 2/2 multi-signature address for the contract. Your " +
                    "Bitcoin program said %s", e.getLocalizedMessage());
            return null;
        }

        /*
         * Send money into the contract
         */
        String contract_load_txid;
        try
        {
            contract_load_txid = bitcoind.sendToAddress(contract_address, contract_amount.toBigDecimal(), contract_name,
                    contract_name);
            out("Successfully sent %s BTC to the multi-signature address for the contract.", contract_amount);
        }
        catch (BitcoinException e)
        {
            out("Sorry, there was a problem sending funds into the multi-signature address for the contract. Your " +
                    "Bitcoin program said %s", e.getLocalizedMessage());
            return null;
        }

        /*
         * Create the redemption transaction
         */
        BTC redemption_amount = contract_amount.subtract(contract_amount_fee);
        String spend_tx;
        try
        {
            System.out.println("redemption_amount " + redemption_amount);
            spend_tx = bitcoin_facade.createSpendFromMultiSig(contract_load_txid, redemption_amount, recipient_address,
                    contract_amount_fee);
        }
        catch (BitcoinException e)
        {
            out("Sorry, there was a problem creating the transaction the recipient will use for redeeming the " +
                    "contract funds. Your Bitcoin program said %s", e.getLocalizedMessage());
            return null;
        }

        /*
         * Create the refund transaction
         */
        BTC refund_amount = contract_amount.subtract(contract_amount_fee);
        String refund_tx;
        try
        {
            System.out.println("refund_amount " + refund_amount);
            refund_tx = bitcoin_facade.createSpendFromMultiSig(contract_load_txid, redemption_amount, sender_address,
                    contract_amount_fee);
        }
        catch (BitcoinException e)
        {
            out("Sorry, there was a problem creating the transaction the recipient will use for refunding the " +
                    "contract funds. Your Bitcoin program said %s", e.getLocalizedMessage());
            return null;
        }

        /*
         * Sign the spend transaction
         */
        String sender_signed_spend_tx;
        try
        {
            sender_signed_spend_tx = bitcoin_facade.signTransactionThatSpendsFromMultiSig(sender_address, spend_tx,
                    contract_load_txid, redeemScript);
        }
        catch (BitcoinException e)
        {
            out("Sorry, there was a problem signing the transaction the recipient will use for redeeming the " +
                    "contract funds. Your Bitcoin program said %s", e.getLocalizedMessage());
            return null;
        }

        /*
         * Sign the refund transaction
         */
        String sender_signed_refund_tx;
        try
        {
            sender_signed_refund_tx = bitcoin_facade.signTransactionThatSpendsFromMultiSig(sender_address, refund_tx,
                    contract_load_txid, redeemScript);
        }
        catch (BitcoinException e)
        {
            out("Sorry, there was a problem signing the transaction the recipient will use for refunding the " +
                    "contract funds. Your Bitcoin program said %s", e.getLocalizedMessage());
            return null;
        }

        return new RecipientData(sender_signed_spend_tx, sender_signed_refund_tx, contract_load_txid, sender_address);
    }

    public static void main(String[] args) throws Exception
    {
        try
        {
            LogManager.getLogManager().readConfiguration(BasicContractExpert.class.getClassLoader()
                    .getResourceAsStream("logging.properties"));
        }
        catch (NullPointerException e)
        {
        }

        Scanner scanner = new Scanner(System.in);
        Console console = System.console();
        if (console == null)
        {
            System.err.println("Can't read the console.");
            System.exit(-1);
        }

        Boolean search = BitcoinD.search();
        boolean testnet;
        if (search == null)
        {
            System.out.print("Cannot connect to your Bitcoin software. This client requires the official " +
                    "Bitcoin software from http://bitcoin.org/en/download\n\n" +
                    "Have you already installed and launched it? (y/N)");
            boolean running = scanner.nextLine().trim().equalsIgnoreCase("y");

            if ( ! running)
            {
                out("Download and install it, load your wallet with some Bitcoin, and rerun this client.");
                return;
            }

            /*
             * Look for the configuration file
             */
            String os_username = System.getProperty("user.name");
            String os_name = System.getProperty("os.name");
            File configuration;
            if(os_name.contains("OS X"))
            {
                new File("/Users/" + os_username + "/Library/Application Support/Bitcoin").mkdirs();
                configuration = new File("/Users/" + os_username + "/Library/Application Support/Bitcoin/bitcoin.conf");
            }
            else if (os_name.contains("Windows XP"))
            {
                configuration = new File("C:\\Documents and Settings\\" + os_username +
                        "\\Application Data\\Bitcoin\\bitcoin.conf");
            }
            else if (os_name.contains("Windows"))
            {
                configuration = new File("C:\\Users\\" + os_username + "\\AppData\\Roaming\\Bitcoin\\bitcoin.conf");
            }
            else
            {
                out("In order to be accessible by this client, Bitcoin must have a configuration file.  " +
                        "Create a configuration file, and allow RPC access, then rerun this client. " +
                        "You can read more about this file at " +
                        "https://en.bitcoin.it/wiki/Running_Bitcoin#Bitcoin.conf_Configuration_File");
                return;
            }

            if (configuration.exists())
           /*
            * If the configuration file is there, maybe it doesn't have server turned on
            */
            {
                out("You appear to have a configuration file already at %s. Edit it and enable RPC activity, " +
                        "by making sure it contains a property\nserver=1\n", configuration.getAbsolutePath());
                return;
            }

           /*
            * If the configuration file is not there, offer to create one
            */
            else
            {
                out("I checked for a configuration file at the expected location, %s, and didn't find it.",
                        configuration.getAbsolutePath());
                out("In order to be accessible by this client, Bitcoin must have a configuration file.  " +
                        "I can create one for you now. You can read more about this file at " +
                        "https://en.bitcoin.it/wiki/Running_Bitcoin#Bitcoin.conf_Configuration_File");
                System.out.print("Create a configuration file? (y/N) ");
                boolean make_conf_file = scanner.nextLine().trim().equalsIgnoreCase("y");

                if (make_conf_file)
                {
                    try
                    {
                        configuration.mkdirs();
                        boolean created = configuration.createNewFile();
                        if ( ! created)
                        {
                            System.out.println("Sorry, I was unable to create your configuration file. ");
                            return;
                        }

                        System.out.print("Set a username for RPC access to your wallet. ");
                        String username = scanner.nextLine();

                        System.out.print("Set a password for RPC access to your wallet. ");
                        String password = scanner.nextLine();

                        Properties properties = new Properties();
                        properties.setProperty("testnet", "0");
                        properties.setProperty("server", "1");
                        properties.setProperty("rpcuser", username);
                        properties.setProperty("rpcpassword", password);
                        properties.store(new FileWriter(configuration), "");
                        System.out.println("Now restart Bitcoin, and rerun this client.");
                        return;
                    }
                    catch (IOException e)
                    {
                        System.out.println("Sorry, I was unable to create your configuration file. " +
                                e.getLocalizedMessage());
                    }
                }
            }

            return;
        }
        else if (search)
        {
            out("Found Bitcoin running.");
            testnet = false;
        }
        else
        {
            out("Found Bitcoin running on the test network.");
            testnet = true;
        }

        BitcoinD bitcoind = null;
        do
            try
            {
                System.out.print("Enter the username for RPC access to your wallet. ");
                String username = scanner.nextLine();

                System.out.print("Enter the password for RPC access to your wallet. ");
                char[] password = console.readPassword();
                bitcoind = new BitcoinD(username, new String(password), testnet);
                Arrays.fill(password, ' ');
            }
            catch (BitcoinDUnavailableException e)
            {
                System.out.println(e.getLocalizedMessage());
            }
        while(bitcoind == null);

        BitcoinFacade facade = new BitcoinFacade(bitcoind);
        BasicContractExpert expert = new BasicContractExpert(facade);

        System.out.println("\nWelcome to the Early Temple client.\n");
        System.out.println("This will help you complete the steps that the sender must perform in step two of the " +
                "protocol described at https://earlytemple.com/index.jsp#protocol\n");

        /*
         * Collect all the user information
         */
        SimpleDateFormat today = new SimpleDateFormat("yyyy-MM-dd");
        String default_operation_name = "Early Temple " + today.format(new Date());
        System.out.print("Would you like to give this contract a name? This will only be used in your own Bitcoin " +
                "transaction records. [" + default_operation_name + "] ");
        String operation_name = scanner.nextLine();
        if (operation_name.isEmpty())
            operation_name = default_operation_name;

        System.out.print("Enter the Bitcoin address you're sending from: ");
        String sender_address = scanner.nextLine();

        System.out.print("Enter the Bitcoin address of the contract recipient: ");
        String recipient_address = scanner.nextLine();

        System.out.print("Enter Early Temple public-key from the contract page: ");
        String et_public_key = scanner.nextLine();

        boolean valid_value = false;
        BTC contract_amount = new BTC(0);
        do
        {
            System.out.print("How much BTC are you entering into the contract? ");
            String contract_amount_string = scanner.nextLine();
            try
            {
                contract_amount = new BTC(contract_amount_string);
                double contract_amount_double = contract_amount.toBigDecimal().doubleValue();
                if (contract_amount_double == 0)
                    System.out.println("Sorry, zero is not a valid value.");
                else if (contract_amount_double < 0)
                    System.out.println("Sorry, contract value cannot be negative.");
                else
                    valid_value = true;
            }
            catch (Exception e)
            {
                System.out.println("Sorry, that is not a valid value.");
            }
        }
        while ( ! valid_value);

        BTC contract_fee = new BTC(0);
        do
        {
            System.out.print("What fee should the spending transaction have? This will be deducted from the contract " +
                    "amount [0.0005] ? ");
            String contract_fee_string = scanner.nextLine();
            if (contract_fee_string.isEmpty())
                contract_fee_string = "0.0005";
            try
            {
                contract_fee = new BTC(contract_fee_string);
                double contract_fee_double = contract_amount.toBigDecimal().doubleValue();
                if (contract_fee_double == 0)
                    System.out.println("Sorry, zero is not a valid value.");
                else if (contract_fee_double < 0)
                    System.out.println("Sorry, contract fee value cannot be negative.");
                else if (contract_fee_double > contract_amount.toBigDecimal().doubleValue())
                    System.out.println("Sorry, contract fee value cannot be larger than the contract value.");
                else
                    valid_value = true;
            }
            catch (Exception e)
            {
                System.out.println("Sorry, that is not a valid value.");
            }
        }
        while ( ! valid_value);

        /*
         * Confirm the input
         */
        out("");
        out("Confirm these values:");
        out("The name of this contract, for your Bitcoin transaction records: " + operation_name);
        out("Your Bitcoin address for this contract: " + sender_address);
        out("Recipient Bitcoin address: " + recipient_address);
        out("Early Temple public key from the contract page: " + et_public_key);
        out("Contract amount: %s BTC", contract_amount);
        out("Contract fee: %s BTC", contract_fee);
        out("The recipient will be receiving %s BTC", contract_amount.subtract(contract_fee));

        out("Are all of these values correct? {y/N)");
        String confirmation = scanner.nextLine().trim();
        if ( ! confirmation.equalsIgnoreCase("y"))
        {
            System.out.println("Aborting without making any changes.");
            System.exit(0);
        }

        System.out.println("OK. Proceeding...");
        RecipientData recipientData =
                expert.senderRoleInBasicContract(operation_name, sender_address, recipient_address, et_public_key,
                        contract_amount, contract_fee);
        if (recipientData == null)
        {
            System.out.println("Exiting.");
            System.exit(0);
        }

        out("");
        out("Success!  Here is the information the receiver needs to redeem this contract through Early Temple:");
        out(recipientData.toString());
    }
}
