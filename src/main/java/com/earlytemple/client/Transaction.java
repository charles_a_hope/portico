package com.earlytemple.client;

public class Transaction
{
    private int confirmations;
    private BTC outputAmountToScriptHash;

    public Transaction(int confirmations, BTC outputAmountToScriptHash)
    {
        this.confirmations = confirmations;
        this.outputAmountToScriptHash = outputAmountToScriptHash;
    }

    public int getConfirmations()
    {
        return confirmations;
    }

    public void setConfirmations(int confirmations)
    {
        this.confirmations = confirmations;
    }

    public BTC getOutputAmountToScriptHash()
    {
        return outputAmountToScriptHash;
    }

    public void setOutputAmountToScriptHash(BTC outputAmountToScriptHash)
    {
        this.outputAmountToScriptHash = outputAmountToScriptHash;
    }
}
