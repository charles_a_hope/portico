/*
 * Copyright 2014 Early Temple
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.earlytemple.client;

import com.sun.istack.internal.Nullable;

/**
 * Represents a transaction output. This is not a complete representation, just the aspects that I need right now.
**/
public class Output
{
    private String hex;
    private String type;
    @Nullable private BTC value;

    public Output(String hex, String type)
    {
        this.hex = hex;
        this.type = type;
    }

    public Output(String hex, String type, BTC value)
    {
        this.hex = hex;
        this.type = type;
        this.value = value;
    }

    public String getHex()
    {
        return hex;
    }

    public void setHex(String hex)
    {
        this.hex = hex;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    @Nullable
    public BTC getValue()
    {
        return value;
    }

    public void setValue(@Nullable BTC value)
    {
        this.value = value;
    }
}
