/*
 * Copyright 2014 Early Temple
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.earlytemple.client;

/**
 * For exceptions thrown by Bitcoind RPC methods.
 */
public class BitcoinException extends Exception
{
    private int rpc_server_code;
    private String rpc_server_message;

    public BitcoinException()
    {
    }

    public BitcoinException(String message)
    {
        super(message);
    }

    /**
     * @param rpc_server_code https://en.bitcoin.it/wiki/Original_Bitcoin_client/API_calls_list#Error_Codes
     * @param rpc_server_message
     */
    public BitcoinException(int rpc_server_code, String rpc_server_message)
    {
        super("Bitcoind error code " + rpc_server_code + ": " + rpc_server_message);
        this.rpc_server_code = rpc_server_code;
        this.rpc_server_message = rpc_server_message;
    }

    public BitcoinException(String format, Object... args)
    {
        super(String.format(format, args));
    }

    public BitcoinException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public BitcoinException(Throwable cause)
    {
        super(cause);
    }

    public int getRpcServerCode()
    {
        return rpc_server_code;
    }

    public String getRpcServerMessage()
    {
        return rpc_server_message;
    }
}
