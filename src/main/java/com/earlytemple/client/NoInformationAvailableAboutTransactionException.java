package com.earlytemple.client;

/**
 * Corresponds to RPC error code -5, "No information available about transaction"
 */
public class NoInformationAvailableAboutTransactionException extends BitcoinException
{
    public NoInformationAvailableAboutTransactionException()
    {
        super(-5, "No information available about transaction");
    }
}
