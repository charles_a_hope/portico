/*
 * Copyright 2014 Early Temple
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.earlytemple.client;

import java.math.*;
import java.text.DecimalFormat;

/**
 * Handling money can be tricky.  This class incorporates several best practices.  Create one of these around a
 * BTC value as soon as possible after it comes back from RPC.
 *
 * <p>This is a decorator around a big decimal number of BTC.</p>
**/
public class BTC
{
    private BigDecimal delegate;

    private BTC(BigDecimal btc)
    {
        delegate = btc;
    }

    public BTC()
    {
    }

    /**
     * JSON RPC API of bitcoind returns its values as Numbers of BTC, instead of integers of Satoshi. Use this
     * factory method for those.
     *
     * @param btc Number of BTC. This is rounded in
     *            <a href="http://en.wikipedia.org/wiki/Rounding#Round_half_to_even">half even</a> style.
     * @throws NumberFormatException if btc value is not a valid number
     */
    public BTC(String btc)
    {
        BigDecimal bigDecimal = new BigDecimal(btc);
        delegate = bigDecimal.setScale(8, RoundingMode.HALF_EVEN);
    }

    /**
     * JSON RPC API of bitcoind returns its values as Numbers of BTC, instead of integers of Satoshi. Use this
     * factory method for those.
     *
     * @param btc Number of BTC. This is rounded in
     *            <a href="http://en.wikipedia.org/wiki/Rounding#Round_half_to_even">half even</a> style.
     */
    public BTC(Number btc)
    {
        this(btc.toString());
    }

    /**
     * Represents this value as a decimal number of BTC
     * @return a String of the decimal format 0.########
     */
    public String toBTC()
    {
        DecimalFormat decimalFormat = new DecimalFormat("0.########");
        return decimalFormat.format(delegate);
    }

    public BigDecimal toBigDecimal()
    {
        return delegate;
    }

    /**
     * Represents this value is an exact number of Satoshi
     */
    public String toSatoshi()
    {
        return delegate.movePointRight(8).toBigInteger().toString();
    }

    /*
     * Delegated methods of BigDecimal
     */

    public BTC subtract(BTC subtrahend)
    {
        return new BTC(delegate.subtract(subtrahend.delegate));
    }

    public BTC add(BTC augend)
    {
        return new BTC(delegate.add(augend.delegate));
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BTC btc = (BTC) o;

        if (delegate != null ? !delegate.equals(btc.delegate) : btc.delegate != null) return false;

        return true;
    }

    public int hashCode()
    {
        return delegate != null ? delegate.hashCode() : 0;
    }

    public String toString()
    {
        return toBTC();
    }

    /**
     * For JSON.
     */
    public BigDecimal getValue()
    {
        return delegate;
    }

    /**
     * For JSON.
     */
    public void setValue(BigDecimal delegate)
    {
        this.delegate = delegate;
    }
}
