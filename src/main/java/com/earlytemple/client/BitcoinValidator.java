/*
 * Copyright 2014 Early Temple
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.earlytemple.client;

/**
 * Allows validation of arguments for {@link BitcoinD} while avoiding potentially sending sketchy data to the server.
 */
public class BitcoinValidator
{
    /**
     * Check if parameter follows the valid address format.
     *
     * <p>
     * 25-34 characters in length, they consist of random digits and uppercase and lowercase letters, with the exception
     * that the uppercase letter "O", uppercase letter "I", lowercase letter "l", and the number "0" are never used to
     * prevent visual ambiguity.
     * </p>
     */
    public boolean isValidAddressFormat(String possible_address)
    {
        if (!possible_address.matches("[0-9A-Za-z&&[^0OIl]]+"))
            return false;

        return !(possible_address.length() < 25 || possible_address.length() > 34);
    }

    /**
     * Check if parameter follows the valid address format.
     *
     * <p>
     * Unsure about the lower size bound.
     * 25(?)-35 characters in length, they consist of random digits and uppercase and lowercase letters, with the
     * exception that the uppercase letter "O", uppercase letter "I", lowercase letter "l", and the number "0" are
     * never used to prevent visual ambiguity.
     * </p>
     */
    public boolean isValidMultisigAddressFormat(String possible_address)
    {
        if (!possible_address.matches("[23][0-9A-Za-z&&[^0OIl]]+"))
            return false;

        return !(possible_address.length() < 25 || possible_address.length() > 35);
    }

    /**
     * Check if the parameter is a legitimate block of hex characters.
     *
     * <p>
     *     Each character should be a hex number, and there should be an even number of them.
     * </p>
     */
    public boolean isValidHexBlock(String possible_hex)
    {
        if (!possible_hex.matches("[0-9a-f]+"))
            return false;

        return possible_hex.length() % 2 != 1;
    }

    /**
     * Check if parameter follows a valid public-key format.
     *
     * <p>
     * Compressed keys begin with {02, 03}, and they are 33 bytes long.
     * Uncompressed keys begin with {04}, and they are 65 bytes long.
     * </p>
     */
    public boolean isValidPublicKeyFormat(String possible_pubkey)
    {
        if (!isValidHexBlock(possible_pubkey))
            return false;

        if (possible_pubkey.startsWith("02") || possible_pubkey.startsWith("03"))
            if (possible_pubkey.length() == 33 * 2)
                return true;

        if (possible_pubkey.startsWith("04"))
            if (possible_pubkey.length() == 65 * 2)
                return true;

        return false;
    }

    /**
     * Check if the parameter meets the minimum standards for the valid transaction format.
     *
     * <p>
     * It should be a valid hex number, and no bigger than 100000 characters.
     * </p>
     */
    public boolean isValidTransactionFormat(String possible_transaction)
    {
        if (!isValidHexBlock(possible_transaction))
            return false;

        if (possible_transaction.length() > 100000)
            return false;

        return possible_transaction.length() >= 50;
    }

    /**
     * Check if the parameter follows the valid transaction ID format.
     *
     * <p>
     * It should be a valid hex number, and have a length of 64 characters.
     * </p>
     */
    public boolean isValidTransactionIDFormat(String possible_transaction_id)
    {
        return isValidHexBlock(possible_transaction_id) && (possible_transaction_id.length() == 64);
    }
}