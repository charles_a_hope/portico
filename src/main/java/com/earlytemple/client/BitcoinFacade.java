/*
 * Copyright 2014 Early Temple
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.earlytemple.client;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides useful, coarse grained methods over {@link BitcoinD}.
 *
 * Thread safe and application scoped.
**/
public class BitcoinFacade
{
    private final Logger logger = LoggerFactory.getLogger(BitcoinFacade.class);
    private BitcoinD bitcoind;
    BitcoinValidator validator = new BitcoinValidator();

    public BitcoinFacade(BitcoinD bitcoind)
    {
        this.bitcoind = bitcoind;
    }

    /**
     * In case the caller needs to drop down to the lower-level and call the basic methods of BitcoinD.
     */
    public BitcoinD getBitcoind()
    {
        return bitcoind;
    }

    /**
     * Creates and adds a nrequired-to-sign multisignature address to the wallet, and returns both the new address,
     * and its redeem script.
     *
     * @param nrequired the number of signatures required to spend from this address
     * @param account A label for this address. It is added to the address book so payments received with the address
     *                will be credited to <code>account</code>.
     * @param keys bitcoin addresses or hex-encoded public keys
     * @return an object containing the new address and its associated
     * redeem script, needed to create transactions that can spend the money.
     * @throws BitcoinException if the response was other than successful
     * @calledBy server-side contract creation
     */
    public MultiSigAddress createAndAddMultiSigAddress(int nrequired, String account, String... keys)
            throws BitcoinException
    {
        bitcoind.addMultiSigAddress(nrequired, account, keys);
        return bitcoind.createMultiSigAddress(nrequired, keys);
    }

    /**
     * Sends the <code>et_service_fee</code> to the <code>et_address</code>, and returns the txid.
     * Transaction fee is controlled by the settings of bitcoin QT.
     *
     * @param download transfer object containing the <code>et_service_fee</code> and the <code>et_address</code>
     * @return txid of the transaction
     * @calledBy client-side contract creation
     */
    public String sendEarlyTempleServiceFee(SenderPaymentDownload download) throws BitcoinException
    {
        logger.trace("sendEarlyTempleServiceFee invoked with " + download);
        BTC et_service_fee = download.getEarlyTempleServiceFee();
        String et_address = download.getEarlyTempleAddress();
        String contract_name = download.getContractName();
        String comment = "Service fee for Early Temple processing of contract \"" + contract_name + "\".";
        String comment_to = "Early Temple fee for contract \"" + contract_name + "\"";
        String txid = bitcoind.sendToAddress(et_address, et_service_fee.toBigDecimal(), comment, comment_to);
        logger.trace("sendEarlyTempleServiceFee returning transaction ID " + txid);
        return txid;
    }

    /**
     * Sends the <code>contract_amount</code> to the <code>contract_address</code>, and returns the txid.
     * Transaction fee is controlled by the settings of bitcoin QT.
     *
     * @param download transfer object containing the <code>contract_amount</code>
     * @param contract_address the bitcoin address of the contract (probably a 2/2 composed of the sender's address
     *                         and the Early Temple's address)
     * @return txid of the transaction
     * @calledBy client-side contract creation
     */
    public String sendContractedAmountToEscrow(SenderPaymentDownload download, String contract_address) throws BitcoinException
    {
        logger.trace("sendContractedAmountToEscrow invoked with " + download);
        BTC contract_amount = download.getContractAmount();
        String contract_name = download.getContractName();
        String comment = "Amount contracted for Early Temple contract \"" + contract_name + "\".";
        String comment_to = "Early Temple contract \"" + contract_name + "\"";
        String txid = bitcoind.sendToAddress(contract_address, contract_amount.toBigDecimal(), comment, comment_to);
        logger.trace("sendContractedAmountToEscrow returning transaction ID " + txid);
        return txid;
    }

    private List<Output> getOutputs(JSONObject jsonObject) throws BitcoinException
    {
        List<Output> outputs =  new ArrayList<Output>();
        JSONArray vouts = (JSONArray) jsonObject.get("vout");

        for (Object o : vouts)
        {
            JSONObject vout = (JSONObject) o;
            JSONObject scriptPubKey = (JSONObject) vout.get("scriptPubKey");
            String hex = (String) scriptPubKey.get("hex");
            String type = (String) scriptPubKey.get("type");
            BTC value = new BTC((Number) vout.get("value"));

            Output output = new Output(hex, type, value);
            outputs.add(output);
        }

        return outputs;
    }

    /**
     * Extract the outputs from a transaction.
     *
     * @param txid transaction id
     * @return unordered List of {@link Output}
     * @throws BitcoinException
     */
    public List<Output> getOutputs(String txid) throws BitcoinException
    {
        String raw_load_transaction = bitcoind.getRawTransaction(txid);
        JSONObject jsonObject = bitcoind.decodeRawTransaction(raw_load_transaction);
        return getOutputs(jsonObject);
    }

    /**
     * Creates a raw transaction that spends from a multi-signature address.
     *
     * @param escrow_load_txid transaction ID of the transaction loading the multi-signature address
     * @param escrowed_amount The exact amount that was loaded into the multi-signature address
     * @param receiver_address the address receiving the money
     * @param transaction_fee the desired fee for bitcoin processing
     * @return a raw transaction ready for signing by one or more of the addresses authorized for the multi-signature
     * address
     * @calledBy client, twice per contract.  Once for success(to receiver), once for failure (to sender)
     * @todo unit test
     */
    public String createSpendFromMultiSig(String escrow_load_txid, BTC escrowed_amount, String receiver_address,
                                          BTC transaction_fee) throws BitcoinException
    {
        logger.trace("createSpendFromMultiSig, with escrow_load_txid {}, escrowed_amount {}, receiver_address {}, " +
                "transaction_fee {}", escrow_load_txid, escrowed_amount, receiver_address, transaction_fee);

        /*
         * Find out which output of the previous transaction paid to the script hash
         */
        logger.trace("Inspecting the outputs of the tx {}.", escrow_load_txid);
        List<Output> outputs = getOutputs(escrow_load_txid);
        int i = 0;
        for (; i < outputs.size(); i++)
        {
            Output output = outputs.get(i);
            if (output.getType().equals("scripthash"))
            {
                logger.trace("Output {} appears to be the script hash that loads the multi-signature address.", i);
                break;
            }
        }

        return bitcoind.createRawTransaction(escrow_load_txid, i, receiver_address,
                (escrowed_amount.subtract(transaction_fee)).toBigDecimal());
    }


    /**
     * A transaction that spends from a multi-signature address must be signed by several of the addresses used
     * to create it.
     *
     * This process is compatible with bitcoind,  but might not be with bitcoin J, because it relies on a
     * spend from a multi-signature address represented as a P2SH.
     *
     * @return a raw spending transaction, signed by the signing address
     * @calledBy client, and server
     * @todo unit test
     */
    public String signTransactionThatSpendsFromMultiSig(String signing_address,
                                                        String raw_transaction_that_spends_from_multisig,
                                                        String escrow_load_txid, String redeem_script)
            throws BitcoinException
    {
        logger.trace("Entering signTransactionThatSpendsFromMultiSig with signing address {}, raw transaction {}, " +
                "load txid {}, redeem script {}.", signing_address, raw_transaction_that_spends_from_multisig,
                escrow_load_txid, redeem_script);

        if (!  validator.isValidAddressFormat(signing_address))
            throw new BitcoinException("Sorry, that address is invalid.");

        if (!  validator.isValidTransactionFormat(raw_transaction_that_spends_from_multisig))
            throw new BitcoinException("Sorry, that transaction is invalid.");

        if (!  validator.isValidTransactionIDFormat(escrow_load_txid))
            throw new BitcoinException("Sorry, that transaction ID is invalid.");

        /*
         * Extract the private key from the signing address
         */
        String private_key = bitcoind.dumpPrivKey(signing_address);

        /*
         * Extract the ScriptPubKeyHex from the load transaction
         */
        logger.trace("Inspecting the outputs of the tx {}.", escrow_load_txid);
        List<Output> outputs = getOutputs(escrow_load_txid);
        String scriptPubKeyHex = null;
        int i = 0;
        for (; i < outputs.size(); i++)
        {
            Output output = outputs.get(i);
            if (output.getType().equals("scripthash"))
            {
                scriptPubKeyHex = output.getHex();
                logger.trace("Output {} appears to be the script hash that loads the multi-signature address, and " +
                        "contains hex {}.", i, scriptPubKeyHex);
                break;
            }
        }

        if (scriptPubKeyHex == null)
            throw new BitcoinException("Couldn't find a scripthash output to sign.");
        logger.trace("scriptPubKeyHex length is " + scriptPubKeyHex.length());

        /*
         * Sign the transaction, and return it
         */
        return bitcoind.signRawTransaction(raw_transaction_that_spends_from_multisig, escrow_load_txid, i,
                scriptPubKeyHex, redeem_script, private_key);
    }

    /**
     * Returns the number of network confirmations for the most recent transaction received by the given address.
     *
     * @param address this address must be owned by this wallet, or else this method will return null
     * @return null if no transactions were yet received by this address, or the address is not owned by this wallet
     */
    public Integer getConfirmationsForLatestTransactionReceivedBy(String address)
    {
        return null;
    }

    /**
     * Returns the number of confirmations of this tx. If the bitcoind instance is not configured for
     */
    public int getConfirmations(String txid) throws BitcoinException
    {
        JSONObject rawTransactionVerbose = bitcoind.getRawTransactionVerbose(txid);
        return (Integer) rawTransactionVerbose.get("confirmations");
    }

    public Transaction getTransaction(String txid) throws BitcoinException
    {
        JSONObject rawTransactionVerbose = bitcoind.getRawTransactionVerbose(txid);
        Integer confirmations = (Integer) rawTransactionVerbose.get("confirmations");
        if (confirmations == null)
            confirmations = 0;
        List<Output> outputs = getOutputs(rawTransactionVerbose);

        for (int i = 0; i < outputs.size(); i++) // todo java 8 way
        {
            Output output = outputs.get(i);
            if (output.getType().equals("scripthash")) // todo check that it points to this contract?
                return new Transaction(confirmations, output.getValue());
        }

        throw new BitcoinException("Strange Transaction! It is lacking any P2SH output.");
    }
}
