/*
 * Copyright 2014 Early Temple
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.earlytemple.client;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;


/**
 * RPC client for a locally running instance of <a href="https://en.bitcoin.it/wiki/Bitcoind">bitcoind</a> or
 * <a href="https://en.bitcoin.it/wiki/Bitcoin-Qt">Bitcoin-Qt</a>.
 *
 * Most public methods correspond to
 * <a href="https://en.bitcoin.it/wiki/Original_Bitcoin_client/API_calls_list">RPC calls</a>.
 *
 * Requires Bitcoind 0.7 or later.
 *
 * Thread safe and application scoped.
**/
public class BitcoinD
{
    private static final Logger logger = LoggerFactory.getLogger(BitcoinD.class);
    static final String FOREIGN_TX_TESTNET = "44b9f9ff28fbfc33d4824729b2ec6f2929588faef81717e194fd7fe2a6b5d384";
    static final String FOREIGN_TX = "b90b3b9a0afaf555078b207e47715e0565e9d6643c17cf07972809b62bd995e7";
    private static final int PRODUCTION_BITCOIN_PORT = 8332;
    private static final int TESTNET_BITCOIN_PORT = 18332;

    private boolean testnet;
    private URL bitcoind_rpc_url;
    public static final Charset QUERY_CHARSET = Charset.forName("ISO8859-1");
    private String credentials;
    BitcoinValidator validator = new BitcoinValidator();

    private BitcoinD(String host, String username, String password, int port) throws BitcoinDUnavailableException
    {
        try
        {
            bitcoind_rpc_url = new URL("http://"+ host + ":" + port + "/");
            logger.debug("Looking for bitcoind at RPC URL " + bitcoind_rpc_url + "...");

            credentials = new BASE64Encoder().encode((username + ":" + password).getBytes());
        }
        catch (MalformedURLException e)
        {
            throw new BitcoinDUnavailableException(e);
        }
    }

    /**
     * Visible for testing
     */
    BitcoinD(String username, String password, int port) throws BitcoinDUnavailableException
    {
        this("127.0.0.1", username, password, port);
    }

    /**
     * Check if the server is up and running.
     *
     * @throws BitcoinDUnavailableException if there was a problem logging into the server
     */
    private void confirmLogin() throws BitcoinDUnavailableException
    {
        try
        {
            decodeRawTransaction(
                    "01000000013b4795719bfa5f04e80ecbe33d4a05ef210585d161577df0f9a8baead44ca0c90000000000ffffffff01f0874b00000000001976a9143873b3b2a3dd968cf3b54e49aa91e84bfa6e369d88ac00000000");
        }
        catch (BitcoinException e)
        {
            throw new BitcoinDUnavailableException("Incorrect credentials.");
        }
    }

    private static boolean isBitcoinProductionDaemonRunning()
    {
        try
        {
            logger.debug("Searching for bitcoind on production network at localhost...");
            new Socket("127.0.0.1", PRODUCTION_BITCOIN_PORT);
            logger.debug("Found bitcoind.");
            return true;
        }
        catch (IOException e)
        {
            logger.debug("Did not find bitcoind.");
            return false;
        }
    }

    private static boolean isBitcoinTestnetDaemonRunning()
    {
        try
        {
            logger.debug("Searching for bitcoind on test network at localhost...");
            new Socket("127.0.0.1", TESTNET_BITCOIN_PORT);
            logger.debug("Found bitcoind.");
            return true;
        }
        catch (IOException e)
        {
            logger.debug("Did not find bitcoind.");
            return false;
        }
    }

    private boolean isBitcoinTestnetDaemonRunning(boolean testnet)
    {
        return testnet ? isBitcoinTestnetDaemonRunning() : isBitcoinProductionDaemonRunning();
    }

    /**
     * Connects to a running instance of bitcoind.
     *
     * @param host running bitcoind
     * @param username for bitcoind RPC account. Set in bitcoin.conf
     * @param password for bitcoind RPC account. Set in bitcoin.conf
     * @param testnet true if bitcoind is running on testnet, false if running on production network
     * @throws BitcoinDUnavailableException if there was a problem logging into the server
     */
    public BitcoinD(String host, String username, String password, boolean testnet) throws BitcoinDUnavailableException
    {
        this(host, username, password, testnet ? TESTNET_BITCOIN_PORT : PRODUCTION_BITCOIN_PORT);
        this.testnet = testnet;
        if (!isBitcoinTestnetDaemonRunning(testnet))
            throw new BitcoinDUnavailableException("Sorry, the " + (testnet ? "testnet"  : "production") +
                    " daemon isn't running.");
        confirmLogin();
    }

    /**
     * Connects to an instance of bitcoind running on localhost.
     *
     * @param username for bitcoind RPC account. Set in bitcoin.conf
     * @param password for bitcoind RPC account. Set in bitcoin.conf
     * @param testnet true if bitcoind is running on testnet, false if running on production network
     * @throws BitcoinDUnavailableException if there was a problem logging into the server
     */
    public BitcoinD(String username, String password, boolean testnet) throws BitcoinDUnavailableException
    {
        this("127.0.0.1", username, password, testnet ? TESTNET_BITCOIN_PORT : PRODUCTION_BITCOIN_PORT);
        this.testnet = testnet;
        if (!isBitcoinTestnetDaemonRunning(testnet))
            throw new BitcoinDUnavailableException("Sorry, the " + (testnet ? "testnet"  : "production") +
                    " daemon isn't running.");
        confirmLogin();
    }


    /**
     * Looks for a locally running instance of bitcoind.
     *
     * @return <code>true</code> if a locally running instance of bitcoind was found on the production network,
     * <code>false</code> if it was found on the test network,
     * <code>null</code> if no instance was found running
     */
    public static Boolean search()
    {
        if (isBitcoinProductionDaemonRunning())
            return true;

        if (isBitcoinTestnetDaemonRunning())
            return false;

        return null;
    }

    /**
     * Load an input stream into a String.
     *
     * @throws IOException
     */
    private String readResponse(InputStream in) throws IOException
    {
        String response_string;ByteArrayOutputStream o = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        for(;;)
        {
            int nr = in.read(buffer);

            if (nr == -1)
                break;
            if (nr == 0)
                throw new IOException("Read timed out");

            o.write(buffer, 0, nr);
        }
        response_string = o.toString();
        return response_string;
    }

    private synchronized Object invoke(String method, Object... params) throws BitcoinException
    {
        return invoke(method, Arrays.asList(params));
    }

    /**
     * @param method one of the method names of the Bitcoin
     *               <a href="https://en.bitcoin.it/wiki/Original_Bitcoin_client/API_calls_list">RPC API</a>
     * @param params a List of the method parameters, as Numbers and Strings
     * @return "result" field returned by server.  This usually contains the interesting return value
     * @throws BitcoinException if the response was other than successful
     */
    private synchronized Object invoke(String method, List<Object> params) throws BitcoinException
    {
        String request_id = UUID.randomUUID().toString(); // ID of response must correspond to ID of request
        JSONRPC2Request jsonrpc2Request = new JSONRPC2Request(method, params, request_id);
        String jsonString = jsonrpc2Request.toString();

        HttpURLConnection connection = null;
        logger.trace("Executing request to " + bitcoind_rpc_url + " with request ID " + request_id);
        String response_string;
        try
        {
            connection = (HttpURLConnection) bitcoind_rpc_url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("Authorization", "Basic " + credentials);
            connection.getOutputStream().write(jsonString.getBytes(QUERY_CHARSET));
            connection.getOutputStream().close();
            int responseCode = connection.getResponseCode();

            response_string = readResponse(connection.getInputStream());
            logger.trace("Retrieved response from bitcoind.");

            if (responseCode != 200)
                throw new BitcoinException(response_string);
        }
        catch (java.net.ConnectException e)
        {
            throw new BitcoinException("Problem connecting to Bitcoind RPC server. Is it running? Did you enable it using" +
                    " a configuration file? Check the client page for help.", e);
        }
        catch (FileNotFoundException e)
        {
            throw new BitcoinException("Problem connecting to Bitcoind RPC server.  It could be too old a version. " +
                    "This client requires version Bitcoind 0.7 or later.", e);
        }
        catch (IOException e)
        {
            if (e.getMessage().contains(" 401 "))
                throw new BitcoinException("Incorrect username or password.", e);
            else
            {
                if (connection != null)
                    try
                    {
                        String error_message = readResponse(connection.getErrorStream());
                        JSONObject parse = (JSONObject) JSONValue.parse(error_message);
                        JSONObject error = (JSONObject) parse.get("error");
                        Number server_code = (Number) error.get("code");
                        String message = (String) error.get("message");

                        if (server_code.equals(-5) &&
                                message.equals("No information available about transaction"))
                            throw new NoInformationAvailableAboutTransactionException();

                        throw new BitcoinException(server_code.intValue(), message);
                    }
                    catch (IOException e1)
                    {
                        // intentionally fall through to next throw statement
                    }
                throw new BitcoinException(e);
            }
        }

        Object parse = JSONValue.parse(response_string);
        if ( ! (parse instanceof JSONObject))
            throw new BitcoinException("Unsure if last operation succeeded. Bitcoind said " + response_string);

        JSONObject return_value = (JSONObject) parse;
        if (return_value.containsKey("error") && return_value.get("error") != null)
            throw new BitcoinException("Operation failed: " + return_value.get("error"));

        if (return_value.containsKey("id") && return_value.get("id") != null)
        {
            if ( ! request_id.equals(((String) return_value.get("id")).trim()))
                throw new BitcoinException("Operation failed. Bitcoind returned response with wrong ID. " +
                        "Actual %s, expected %s.", return_value.get("id"), request_id);
        }
        else
            throw new BitcoinException("Unsure if last operation succeeded. Bitcoind did not return a JSON response ID.");

        return return_value.get("result");
    }

    /**
     * Sends some BTC to an address.
     *
     * @param address bitcoin address to receive the amount.
     * @param amount amount to be sent. Rounded to 8 decimal places.
     * @param comment comment associated with this transaction, visible in Bitcoin QT
     * @param comment_to comment associated with this transaction in the <code>to</code> field, visible in Bitcoin QT
     * @return transaction ID
     * @throws BitcoinException if the response was other than successful
     */
    public String sendToAddress(String address, BigDecimal amount, String comment, String comment_to)
            throws BitcoinException
    {
        logger.debug("Sending {} BTC to {}...", amount, address);
        String txid = (String) invoke("sendtoaddress", address.trim(), amount, comment, comment_to);
        logger.debug("Sending {} BTC to {} succeeded, with transaction ID {}", amount, address, txid);
        return txid;
    }

    /**
     * Gets information about a bitcoin address. Most useful for returning the public key.
     *
     * @param address a bitcoin address owned by the wallet in the server
     * @return a JSON object that looks like <code>{"isvalid":true,
     * "address":"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD",
     * "ismine":true,
     * "isscript":false,
     * "pubkey":"032f02808dac9ddba1da0551e1892c21ca9e5f54348da4057400cdeaf0716d9c28",
     * "iscompressed":true,
     * "account":"tests 2"
     * }</code>
     * @throws BitcoinException if the response was other than successful
     */
    public JSONObject validateAddress(String address) throws BitcoinException
    {
        logger.debug("Validating {}...", address);
        JSONObject json = (JSONObject) invoke("validateaddress", address.trim());
        logger.debug("Validating {} succeeded.  Response: {}", address, json);
        return json;
    }

    /**
     * Returns the public key associated with this address.
     *
     * @param address a bitcoin address owned by the wallet in the server
     * @throws BitcoinException if the response was other than successful
     */
    public String getPublicKey(String address) throws BitcoinException
    {
        JSONObject jsonObject = validateAddress(address);
        return (String) jsonObject.get("pubkey");
    }

    /**
     * Creates a nrequired-to-sign multisignature address, but does not add it to the wallet.
     *
     * @param nrequired the number of signatures required to spend from this address
     * @param keys_or_addresses hex-encoded public keys of the participants (or addresses), maximum of 20
     * @return an object containing the new address and its associated redeem script, which is needed to create
     * transactions that can spend the money.
     * @throws BitcoinException if the response was other than successful
     */
    public MultiSigAddress createMultiSigAddress(int nrequired, String... keys_or_addresses) throws BitcoinException
    {
        if (nrequired > keys_or_addresses.length)
            throw new BitcoinException("Sorry, the number required must be equal to less than the number of keys.");

        if (nrequired < 1)
            throw new BitcoinException("Sorry, the number required must be greater than zero!");

        if (keys_or_addresses.length > 20)
            throw new BitcoinException("Sorry, the maximum number of keys is 20.");

        for (String key_or_address : keys_or_addresses)
            if ( ! validator.isValidAddressFormat(key_or_address) && ! validator.isValidPublicKeyFormat(key_or_address))
                throw new BitcoinException("Sorry, not a valid Bitcoin address or public key.");

        List<String> keys_list = Arrays.asList(keys_or_addresses);
        logger.debug("Creating multi-signature address requiring {} of public keys " + keys_list, nrequired);
        JSONObject json = (JSONObject) invoke("createmultisig", nrequired, keys_list);
        String multi_sig_address = (String) json.get("address");
        String multi_sig_redeem_script = (String) json.get("redeemScript");
        logger.debug("Creating multi-signature address {} succeeded.", multi_sig_address);
        return new MultiSigAddress(multi_sig_address, multi_sig_redeem_script);
    }

    /**
     * Produces a human-readable JSON object for a raw transaction.
     *
     * @param raw_transaction in hex format
     * @return a JSON object that looks like
     * <code>{ "locktime" : 0,
       "txid" : "c9a04cd4eabaa8f9f07d5761d1850521ef054a3de3cb0ee8045ffa9b7195473b",
       "version" : 1,
       "vin" : [ { "scriptSig" : { "asm" : "3045022100949632739a5f75db32e7b945827608bb7a41ab119c6cef5c2749d5f4111a44d5022038db89fc3e94916df99ebdf28c798c6140ef93144f22a37a6624dcee34584d9701 02e1a2787ca8e25683f08316c03943a15c8734c4727ebbe15a0bc0dc5fe03f945a",
                 "hex" : "483045022100949632739a5f75db32e7b945827608bb7a41ab119c6cef5c2749d5f4111a44d5022038db89fc3e94916df99ebdf28c798c6140ef93144f22a37a6624dcee34584d97012102e1a2787ca8e25683f08316c03943a15c8734c4727ebbe15a0bc0dc5fe03f945a"
               },
             "sequence" : 4294967295,
             "txid" : "3d0a15c851a969d9a94baf21723da7d636c60a8acba366d530b26a002859354b",
             "vout" : 0
           } ],
       "vout" : [ { "n" : 0,
             "scriptPubKey" : { "addresses" : [ "2N63PJdntXukLZ9WkmzhL9vpuWhx8FwSyfu" ],
                 "asm" : "OP_HASH160 8c5c6bab90f4552a7a5f81f1e671cf028ff76881 OP_EQUAL",
                 "hex" : "a9148c5c6bab90f4552a7a5f81f1e671cf028ff7688187",
                 "reqSigs" : 1,
                 "type" : "scripthash"
               },
             "value" : 0.05
           },
           { "n" : 1,
             "scriptPubKey" : { "addresses" : [ "movNtCdKgKCxEeDUXkPCk7aThdGqLVfyGA" ],
                 "asm" : "OP_DUP OP_HASH160 5c2f195adad1c2aff030ee7e7958360b0fdac456 OP_EQUALVERIFY OP_CHECKSIG",
                 "hex" : "76a9145c2f195adad1c2aff030ee7e7958360b0fdac45688ac",
                 "reqSigs" : 1,
                 "type" : "pubkeyhash"
               },
             "value" : 0.8495
           }
         ]
     }</code>
     */
    public JSONObject decodeRawTransaction(String raw_transaction) throws BitcoinException
    {
        logger.debug("About to decode raw transaction " + raw_transaction);
        JSONObject json = (JSONObject) invoke("decoderawtransaction", raw_transaction);
        logger.debug("Decoded raw transaction into " + json);
        return json;
    }

    /**
     * @see <a href='http://bitcoin.stackexchange.com/questions/11759/get-non-wallet-transactions-using-bitcoin-rpc-gettransaction'>Get non-wallet transactions using bitcoin rpc (gettransaction)</a>
     * @return true if this node has indexed transactions that weren't created in this wallet.
     */
    public boolean hasTxIndex()
    {
        String foreign_tx = testnet ? FOREIGN_TX_TESTNET : FOREIGN_TX; // a tx not created by this wallet

        try
        {
            getRawTransaction(foreign_tx);
            return true;
        }
        catch (BitcoinException e)
        {
            return false;
        }
    }

    /**
     * Returns raw transaction representation for given transaction id.
     *
     * @param txid transaction ID
     * @return hex string
     */
    public String getRawTransaction(String txid) throws BitcoinException
    {
        logger.debug("Getting raw transaction for txid " + txid);
        String raw_transaction = (String) invoke("getrawtransaction", txid);
        logger.debug("Got raw transaction " + raw_transaction);
        return raw_transaction;
    }

    /**
     * Returns a detailed version of the tx.
     *
     * @throws BitcoinException if the tx wasn't made from this wallet and this wallet isn't configured to index
     * foreign tx. (See {@link #hasTxIndex()} about configuring this wallet to index foreign tx.
     */
    public JSONObject getRawTransactionVerbose(String txid) throws BitcoinException
    {
        logger.debug("Getting raw transaction for txid " + txid);
        JSONObject raw_transaction = (JSONObject) invoke("getrawtransaction", txid, 1);
        logger.debug("Got raw transaction " + raw_transaction);
        return raw_transaction;
    }

    /**
     * Creates a raw transaction spending given inputs. Note that the transaction's inputs are not signed, and
     * it is not stored in the wallet or transmitted to the network.
     *
     * @param txid input transaction (todo Create a signature accepting multiple inputs?)
     * @param vout output number of input transaction
     * @param address address to send to
     * @param amount amount to output, in Satoshis. Be sure to reduce this below the input amount, to leave a network fee!
     * @return newly created raw transaction
     */
    public String createRawTransaction(String txid, int vout, String address, BigDecimal amount) throws BitcoinException
    {
        logger.debug("Creating raw transaction with input {}, to {}, for {}.", txid, address, amount);
        List<Map<String, Object>> inputs = new ArrayList<Map<String, Object>>();
        HashMap<String, Object> input = new HashMap<String, Object>();
        inputs.add(input);
        input.put("txid", txid);
        input.put("vout", vout);

        HashMap<String, Object> output = new HashMap<String, Object>();
        output.put(address, amount);

        String raw_transaction = (String) invoke("createrawtransaction", inputs, output);
        logger.debug("Created raw transaction {}", raw_transaction);
        return raw_transaction;
    }

    /**
     * Adds signatures to a raw transaction and returns the resulting raw transaction.
     *
     * @param raw_transaction the transaction being signed
     * @param txid the transaction ID of the input being signed
     * @param vout the index of the input being signed
     * @param scriptPubKey the scriptPubKey of the input being signed
     * @param redeem_script the redeem script of the input being signed
     * @param private_key the private key used to sign this transaction
     * @return the signed raw transaction as a hex string
     * @throws BitcoinException
     */
    public String signRawTransaction(String raw_transaction, String txid, int vout, String scriptPubKey,
                                     String redeem_script, String private_key)
            throws BitcoinException
    {
        logger.debug("Signing output {} of raw transaction {} having txid {}.", vout, raw_transaction, txid);

        List<Map<String, Object>> inputs = new ArrayList<Map<String, Object>>(); // todo accept list of Input instances
        HashMap<String, Object> input = new HashMap<String, Object>();
        inputs.add(input);
        input.put("txid", txid);
        input.put("vout", vout);
        input.put("scriptPubKey", scriptPubKey);
        input.put("redeemScript", redeem_script);

        List<String> private_keys = new ArrayList<String>(); // todo accept multiple private key parameters
        private_keys.add(private_key);

        JSONObject jsonObject = (JSONObject) invoke("signrawtransaction", raw_transaction, inputs, private_keys);
        String signed_raw_transaction = (String) jsonObject.get("hex");
        logger.debug("Signed transaction is " + signed_raw_transaction);
        if (signed_raw_transaction.equals(raw_transaction))
            throw new BitcoinException("Signing appears to have done nothing.");
       return signed_raw_transaction;
    }

    /**
     * Adds signatures to a raw transaction and returns the resulting raw transaction. This signature is for
     * transactions having inputs that have been broadcast already.  For transactions that haven't, use
     * {@link #signRawTransaction(String, String, int, String, String, String)}.
     *
     * @param raw_transaction the transaction being signed
     * @param private_key the private key used to sign this transaction
     * @return the signed raw transaction as a hex string
     * @throws BitcoinException
     */
    public String signRawTransaction(String raw_transaction, String private_key) throws BitcoinException
    {
        logger.debug("Signing raw transaction {}.", raw_transaction);
        List inputs = new ArrayList();// placeholder for the inputs parameter, not used in this signature
        List<String> private_keys = new ArrayList<String>(); // todo accept multiple private key parameters
        private_keys.add(private_key);
        JSONObject jsonObject = (JSONObject) invoke("signrawtransaction", raw_transaction, inputs, private_keys);
        String signed_raw_transaction = (String) jsonObject.get("hex");
        logger.debug("Signed transaction is " + signed_raw_transaction);

        if (signed_raw_transaction.equals(raw_transaction))
            throw new BitcoinException("Signing appears to have done nothing.");
        return signed_raw_transaction;
    }

    /**
     * Returns the <code>scriptPubKey</code> from the first input of the transaction.
     *
     * @param raw_transaction the transaction being analyzed
     * @return the scriptPubKey as a hex string
     * @throws BitcoinException
     */
    public String getScriptPubKeyHex(String raw_transaction) throws BitcoinException
    {
        logger.debug("Getting the scriptPubKey from raw transaction " + raw_transaction);
        JSONObject jsonObject = decodeRawTransaction(raw_transaction);
        JSONArray vouts = (JSONArray) jsonObject.get("vout");
        JSONObject vout = (JSONObject) vouts.get(0);
        JSONObject scriptPubKey = (JSONObject) vout.get("scriptPubKey");
        String hex = (String) scriptPubKey.get("hex");
        logger.debug("scriptPubKey is " + hex);
        return hex;
    }

    /**
     * Reveals the private key corresponding to the specified bitcoin address
     */
    public String dumpPrivKey(String address) throws BitcoinException
    {
        logger.debug("Getting private key for " + address);
        String key = (String) invoke("dumpprivkey", address);
        logger.debug("Got private key for " + address);
        return key;
    }

    /**
     * Submits raw transaction (serialized, hex-encoded) to local node and network.
     *
     * @param raw_transaction the transaction being sent
     * @return transaction ID
     */
    public String sendRawTransaction(String raw_transaction) throws BitcoinException
    {
        logger.debug("Sending raw transaction {}...", raw_transaction);
        String txid = (String) invoke("sendrawtransaction", raw_transaction);
        logger.debug("Sent raw transaction and it created txid {}.", txid);
        return txid;
    }

    /**
     * Creates a new bitcoin address for receiving payments.
     *
     * @param account A label for this address .It is added to the address book so payments received with the address
     *                will be credited to <code>account</code>.
     */
    public String getNewAddress(String account) throws BitcoinException
    {
        logger.debug("Creating a new address for account {}...", account);
        String address = (String) invoke("getnewaddress", account);
        logger.debug("New address {} was created.", address);
        return address;
    }

    /**
     * Get details about a transaction.  This transaction must have been originated by this wallet! Use
     * {@link #getRawTransaction(String)} for transactions originating outside this wallet.
     *
     * @param txid
     * @return JSON object that looks like
     * <code>{ "amount" : 0.0495,
       "blockhash" : "00000000e51f7d96ce0ff80ea2353ae869b4b48d2ed8505d28d9d5275c235e58",
       "blockindex" : 3,
       "blocktime" : 1370390779,
       "confirmations" : 422,
       "details" : [ { "account" : "tests 2",
             "address" : "mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD",
             "amount" : 0.0495,
             "category" : "receive"
           } ],
       "time" : 1370389795,
       "timereceived" : 1370389795,
       "txid" : "c312ec2dbd060cce22c9adb46074e5be3075e5c050e7cd1b929b7d2f3f4538a0"
     }</code>
     * @throws BitcoinException
     */
    public JSONObject getTransaction(String txid) throws BitcoinException
    {
        logger.debug("Getting transaction for txid {}...", txid);
        JSONObject json = (JSONObject) invoke("gettransaction", txid);
        logger.debug("Got transaction {}.", json);
        return json;
    }

    /**
     * Shows all the money that has been received by this wallet, including transactions with zero confirmations.
     *
     * @return an array of JSON objects containing:<ul>
     <li>"address" : receiving address</li>
     <li>"account" : the account of the receiving address</li>
     <li>"amount" : total amount received by the address</li>
     <li>"confirmations" : number of confirmations of the most recent transaction included</li>
     *</ul>
     */
    public JSONArray listReceivedByAddress() throws BitcoinException
    {
        logger.debug("Listing received by address...");
        JSONArray json = (JSONArray) invoke("listreceivedbyaddress", 0);
        logger.debug("Received by address: " + json);
        return json;
    }

    /**
     * Creates and adds a nrequired-to-sign multisignature address to the wallet. Does not return the redeem script.
     *
     * @param nrequired the number of signatures required to spend from this address
     * @param account A label for this address. It is added to the address book so payments received with the address
     *                will be credited to <code>account</code>.
     * @param keys bitcoin addresses or hex-encoded public keys, of the participants
     * @return The newly created address
     * @throws BitcoinException if the response was other than successful
     */
    public String addMultiSigAddress(int nrequired, String account, String... keys) throws BitcoinException
    {
        List<String> keys_list = Arrays.asList(keys);
        logger.debug("Adding multi-signature address requiring {} of public keys {}...", nrequired, keys_list);
        String multi_sig_address = (String) invoke("addmultisigaddress", nrequired, keys_list, account);
        logger.debug("Adding multi-signature address {} succeeded.", multi_sig_address);
        return multi_sig_address;
    }

    public BTC getBalance() throws BitcoinException
    {
        logger.debug("Getting balance...");
        Double balance = (Double) invoke("getbalance");
        logger.debug("Got balance {}.", balance);
        return new BTC(balance);
    }

    public static void main(String[] args) throws Exception
    {
        BitcoinD bitcoind = new BitcoinD("u", "p", true);
        System.out.println(bitcoind.hasTxIndex());



        String tx_id = "1eb0117a53761f93e1fa8c0dfef9380f145221f8936c92c22498db55b198fdd8";
//        JSONObject foreignRawTransaction = bitcoind.getRawTransactionVerbose("44b9f9ff28fbfc33d4824729b2ec6f2929588faef81717e194fd7fe2a6b5d384");
        JSONObject foreignRawTransaction = bitcoind.getRawTransactionVerbose(tx_id);
        System.out.println(foreignRawTransaction);


//        String rawTransaction = bitcoind.getRawTransaction(tx_id);
//        JSONObject jsonObject = bitcoind.decodeRawTransaction(rawTransaction);
//        System.out.println(jsonObject);

//        BTC balance = bitcoind.getBalance();
//        System.out.println(balance);
//
//        String sender_address = bitcoind.getNewAddress("Sender");
//        String recipient_address = bitcoind.getNewAddress("Recipient");
//        MultiSigAddress multiSigAddress = bitcoind.createMultiSigAddress(2, sender_address, recipient_address);
//        System.out.println("multiSigAddress.getAddress() " + multiSigAddress.getAddress());
//
//        String sender_publicKey = bitcoind.getPublicKey(sender_address);
//        System.out.println("sender_publicKey " + sender_publicKey);
//        String recipient_publicKey = bitcoind.getPublicKey(recipient_address);
//        MultiSigAddress multiSigAddress1 = bitcoind.createMultiSigAddress(2, sender_publicKey, recipient_publicKey);
//        System.out.println(multiSigAddress1.getAddress());
//
//        MultiSigAddress multiSigAddress2 = bitcoind.createMultiSigAddress(2, sender_publicKey, recipient_address);
//        System.out.println(multiSigAddress2.getAddress());

//        System.out.println(bitcoind.getRawTransaction("6f48ad0fe3190e1e991ff35e1d75a511ed504d13e20b31c02bf4f00d57efe829"));

//        System.out.println("0450863AD64A87AE8A2FE83C1AF1A8403CB53F53E486D8511DAD8A04887E5B23522CD470243453A299FA9E77237716103ABC11A1DF38855ED6F2EE187E9C582BA6".length()  + " chars");
    }
}
