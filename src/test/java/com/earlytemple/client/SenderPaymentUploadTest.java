/*
 * Copyright 2014 Early Temple
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.earlytemple.client;

import org.junit.Before;
import org.junit.Test;

import net.minidev.json.JSONObject;

import static com.earlytemple.client.SenderPaymentUpload.*;

/**
 *
 **/
public class SenderPaymentUploadTest
{
    JSONObject raw_json;
    SenderPaymentUpload upload;

    @Before
    public void setUp() throws Exception
    {
        /*
         * Create a fully valid JSON block
         */
        raw_json = new JSONObject();
        raw_json.put(SenderPaymentUpload .ET_SERVICE_FEE_TXID, "value");
        raw_json.put(CONTRACT_LOAD_TXID, "value");
        raw_json.put(SUCCESS_TX, "value");
        raw_json.put(FAILURE_TX, "value");
        raw_json.put(SENDER_PUBLIC_KEY, "value");
    }

    @Test
    public void constructionSucceeds() throws Exception
    {
        /*
         * Given a valid block of JSON...
         */

        /*
         * When the JSON constructor is called
         */
        upload = new SenderPaymentUpload(raw_json);

        /*
         * Then no exception should be thrown...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresEtServiceFeeTxid() throws Exception
    {
        /*
         * Given a block of JSON missing the early Temple service fee transaction ID
         */
        raw_json.remove(ET_SERVICE_FEE_TXID);

        /*
         * When the JSON constructor is called
         */
        upload = new SenderPaymentUpload(raw_json);

        /*
         * Then an illegal argument exception should be thrown...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresEtServiceFeeTxidAsAString() throws Exception
    {
        /*
         * Given a block of JSON containing the early Temple service fee transaction ID as a number
         */
        raw_json.put(ET_SERVICE_FEE_TXID, 19.5);

        /*
         * When the JSON constructor is called
         */
        upload = new SenderPaymentUpload(raw_json);

        /*
         * Then an illegal argument exception should be thrown...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresContractLoadTxid() throws Exception
    {
        /*
         * Given a block of JSON missing the Contract Load transaction ID
         */
        raw_json.remove(CONTRACT_LOAD_TXID);

        /*
         * When the JSON constructor is called
         */
        upload = new SenderPaymentUpload(raw_json);

        /*
         * Then an illegal argument exception should be thrown...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresContractLoadTxidAsAString() throws Exception
    {
        /*
         * Given a block of JSON containing the Contract Load transaction ID as a number
         */
        raw_json.put(CONTRACT_LOAD_TXID, 19.5);

        /*
         * When the JSON constructor is called
         */
        upload = new SenderPaymentUpload(raw_json);

        /*
         * Then an illegal argument exception should be thrown...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresSuccessTx() throws Exception
    {
        /*
         * Given a block of JSON missing the success transaction
         */
        raw_json.remove(SUCCESS_TX);

        /*
         * When the JSON constructor is called
         */
        upload = new SenderPaymentUpload(raw_json);

        /*
         * Then an illegal argument exception should be thrown...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresSuccessTxAsAString() throws Exception
    {
        /*
         * Given a block of JSON containing the success transaction as a number
         */
        raw_json.put(SUCCESS_TX, 19.5);

        /*
         * When the JSON constructor is called
         */
        upload = new SenderPaymentUpload(raw_json);

        /*
         * Then an illegal argument exception should be thrown...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresFailureTx() throws Exception
    {
        /*
         * Given a block of JSON missing the failure transaction
         */
        raw_json.remove(FAILURE_TX);

        /*
         * When the JSON constructor is called
         */
        upload = new SenderPaymentUpload(raw_json);

        /*
         * Then an illegal argument exception should be thrown...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresFailureTxAsAString() throws Exception
    {
        /*
         * Given a block of JSON containing the failure transaction as a number
         */
        raw_json.put(FAILURE_TX, 19.5);

        /*
         * When the JSON constructor is called
         */
        upload = new SenderPaymentUpload(raw_json);

        /*
         * Then an illegal argument exception should be thrown...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresSenderPublicKey() throws Exception
    {
        /*
         * Given a block of JSON missing the failure transaction
         */
        raw_json.remove(FAILURE_TX);

        /*
         * When the JSON constructor is called
         */
        upload = new SenderPaymentUpload(raw_json);

        /*
         * Then an illegal argument exception should be thrown...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresSenderPublicKeyAsAString() throws Exception
    {
        /*
         * Given a block of JSON containing the failure transaction as a number
         */
        raw_json.put(FAILURE_TX, 19.5);

        /*
         * When the JSON constructor is called
         */
        upload = new SenderPaymentUpload(raw_json);

        /*
         * Then an illegal argument exception should be thrown...
         */
    }
}
