/*
 * Copyright 2014 Early Temple
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.earlytemple.client;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *
 **/
public class BTCTest
{
    @Test
    public void toBTCFrom0pt2495() throws Exception
    {
        /*
         * Given 0.2495
         */
        double number = 0.2495;

        /*
         * When a BTC is constructed from it
         */
        BTC btc = new BTC(number);

        /*
         * Then toBTC should return the same number
         */
        assertEquals("0.2495", btc.toBTC());

        /*
         * And toSatoshi should return the same number times 1e8
         */
        assertEquals("24950000", btc.toSatoshi());
    }

    @Test
    public void toBTCFrom0pt2495FromString() throws Exception
    {
        /*
         * Given 0.2495
         */
        String number = "0.2495";

        /*
         * When a BTC is constructed from it
         */
        BTC btc = new BTC(number);

        /*
         * Then toBTC should return the same number
         */
        assertEquals("0.2495", btc.toBTC());

        /*
         * And toSatoshi should return the same number times 1e8
         */
        assertEquals("24950000", btc.toSatoshi());
    }

    @Test
    public void toBTCFrom32pt123() throws Exception
    {
        /*
         * Given 32.123
         */
        double number = 32.123;

        /*
         * When a BTC is constructed from it
         */
        BTC btc = new BTC(number);

        /*
         * Then toBTC should return the same number
         */
        assertEquals("32.123", btc.toBTC());

        /*
         * And toSatoshi should return the same number times 1e8
         */
        assertEquals("3212300000", btc.toSatoshi());
    }

    @Test
    public void toBTCFromNegative32pt123() throws Exception
    {
        /*
         * Given -32.123
         */
        double number = -32.123;

        /*
         * When a BTC is constructed from it
         */
        BTC btc = new BTC(number);

        /*
         * Then toBTC should return the same number
         */
        assertEquals("-32.123", btc.toBTC());

        /*
         * And toSatoshi should return the same number times 1e8
         */
        assertEquals("-3212300000", btc.toSatoshi());
    }

    @Test
    public void toBTCFromOneSatoshi() throws Exception
    {
        /*
         * Given 0.00000001
         */
        double number = 0.00000001;

        /*
         * When a BTC is constructed from it
         */
        BTC btc = new BTC(number);

        /*
         * Then toBTC should return one satoshi
         */
        assertEquals("0.00000001", btc.toBTC());

        /*
         * And toSatoshi should return 1
         */
        assertEquals("1", btc.toSatoshi());
    }

    /**
     * 0.4 Satoshi should be rounded down to zero Satoshi
     */
    @Test
    public void toBTCFromOpt4Satoshi() throws Exception
    {
        /*
         * Given 0.000000004
         */
        double number = 0.000000004;

        /*
         * When a BTC is constructed from it
         */
        BTC btc = new BTC(number);

        /*
         * Then toBTC should return zero
         */
        assertEquals("0", btc.toBTC());

        /*
         * And toSatoshi should return zero
         */
        assertEquals("0", btc.toSatoshi());
    }

    /**
     * 0.6 Satoshi should be rounded up to one Satoshi
     */
    @Test
    public void toBTCFromOpt6Satoshi() throws Exception
    {
        /*
         * Given 0.000000006
         */
        double number = 0.000000006;

        /*
         * When a BTC is constructed from it
         */
        BTC btc = new BTC(number);

        /*
         * Then toBTC should return one satoshi
         */
        assertEquals("0.00000001", btc.toBTC());

        /*
         * And toSatoshi should return 1
         */
        assertEquals("1", btc.toSatoshi());
    }

    @Test
    public void add() throws Exception
    {
        /*
         * Given two BTC values
         */
        BTC a = new BTC("11.11");
        BTC b = new BTC("11.111");

        /*
         * When these two are added
         */
        BTC sum = a.add(b);

        /*
         * Then the result should be the sum
         */
        assertEquals("add failed.", new BTC("22.221"), sum);
    }
}
