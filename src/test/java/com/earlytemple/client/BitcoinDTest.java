/*
 * Copyright 2014 Early Temple
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.earlytemple.client;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.util.Random;

import static junit.framework.Assert.*;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;

public class BitcoinDTest
{
    private final Random random = new Random();
    private BitcoinValidator validator;
    private HttpServer webserver;
    private FakeBitcoinDRPCServer rpc_server;

    class FakeBitcoinDRPCServer implements HttpHandler
    {
        boolean was_hit;
        String in_id;
        String in_method;
        JSONObject in_value;
        JSONArray in_params;

        JSONObject out_value = new JSONObject();
        Object out_result;
        int out_return_status = 200;

        public void handle(HttpExchange httpExchange) throws IOException
        {
            was_hit = true;
            InputStreamReader isr =
                    new InputStreamReader(httpExchange.getRequestBody(), "utf-8");
            BufferedReader br = new BufferedReader(isr);
            String query = br.readLine();
            in_value = (JSONObject) JSONValue.parse(query);
            in_id = (String) in_value.get("id");
            in_method = (String) in_value.get("method");
            in_params = (JSONArray) in_value.get("params");

            out_value.put("id", in_id);
            if (out_return_status == 200)
            {
                out_value.put("result", out_result);
                out_value.put("error", null);
            }
            else
                out_value.put("error", out_result);

            String response_string = out_value.toJSONString();
            httpExchange.sendResponseHeaders(out_return_status, response_string.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response_string.getBytes());
            os.close();
        }
    }

    private BitcoinD client;

    @Before
    public void setUp() throws Exception
    {
        webserver = HttpServer.create(new InetSocketAddress(18123), 0);
        rpc_server = new FakeBitcoinDRPCServer();
        webserver.createContext("/", rpc_server);
        webserver.setExecutor(null);
        webserver.start();

        validator = mock(BitcoinValidator.class);
        given(validator.isValidAddressFormat(anyString())).willReturn(true);
        given(validator.isValidPublicKeyFormat(anyString())).willReturn(true);
        given(validator.isValidTransactionFormat(anyString())).willReturn(true);

        client = new BitcoinD("username", "password", 18123);
        client.validator = validator;
    }

    @After
    public void tearDown() throws Exception
    {
        webserver.stop(0);
    }

    private String randomHex()
    {
        return Long.toHexString(Math.abs(random.nextLong()));
    }

    @Test
    public void sendToAddressSucceeds() throws Exception
    {
        /*
         * Given an address and amount, and comments
         */
        String address = randomAlphanumeric(34);
        String amount = "9310.9853579";
        String comment = randomAlphanumeric(64);
        String comment_to = randomAlphanumeric(64);

        /*
         * And the server responds with a transaction ID in the result field of the JSON returned
         */
        String txid = randomAlphanumeric(64);
        rpc_server.out_result = txid;

        /*
         * When sendToAddress is called with these parameters
         */
        String actual_txid = client.sendToAddress(address, new BigDecimal(amount), comment, comment_to);


        /*
         * Then the server should have been hit, and with the method "sendtoaddress"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "sendtoaddress", rpc_server.in_method);

        /*
         * And the correct parameters should have been sent
         */
        assertEquals("Wrong value for address parameter.", address, ((String) rpc_server.in_params.get(0)));
        assertEquals("Wrong value for amount parameter.", amount, ((Number) rpc_server.in_params.get(1)).toString());
        assertEquals("Wrong value for comment parameter.", comment, ((String) rpc_server.in_params.get(2)));
        assertEquals("Wrong value for comment to parameter.", comment_to, ((String) rpc_server.in_params.get(3)));

        /*
         * And sendToAddress should have returned the transaction ID output by the server
         */
        assertEquals("Wrong transaction ID returned.", txid, actual_txid);
    }

    @Test(expected = BitcoinException.class)
    public void sendToAddressThrowsBitcoinExceptionIfServerReturns400() throws Exception
    {
        /*
         * Given that the server returns a status of 400
         */
        JSONObject result = new JSONObject();
        result.put("code", -8);
        result.put("message", "Invalid, missing or duplicate parameter");
        rpc_server.out_result = result;
        rpc_server.out_return_status = 400;

        /*
         * And given an address and amount, and comments
         */
        String address = randomAlphanumeric(34);
        String amount = randomNumeric(5) + "." + randomNumeric(8);
        String comment = randomAlphanumeric(64);
        String comment_to = randomAlphanumeric(64);

        /*
         * When sendToAddress is called with these parameters
         */
        client.sendToAddress(address, new BigDecimal(amount), comment, comment_to);

        /*
         * Then the client should throw BitcoinException...
         */
    }

    @Test
    public void getPublicKeySucceeds() throws Exception
    {
        /*
         * Given that the server returns a response, for validate address, containing a certain public key
         */
        String public_key = "032f02808dac9ddba1da0551e1892c21ca9e5f54348da4057400cdeaf0716d9c28";
        JSONObject result = new JSONObject();
        result.put("pubkey", public_key);
        rpc_server.out_result = result;

        /*
         * When getPublicKey is called
         */
        String actual_public_key = client.getPublicKey("address");

        /*
         * Then it should return that public-key
         */
        assertEquals("Wrong public-key returned.", public_key, actual_public_key);
    }

    @Test
    public void createMultiSigSucceedsWithKeys() throws Exception
    {
        /*
         * Given two keys
         */
        String key = randomHex();
        String key1 = randomHex();

        /*
         * And that the validator accepts the input as keys, but rejects them as addresses
         */
        given(validator.isValidAddressFormat(anyString())).willReturn(false);
        given(validator.isValidPublicKeyFormat(anyString())).willReturn(true);

        /*
         * And a number of required addresses to redeem
         */
        int nrequired = 2;

        /*
         * And the server responds with JSON including an address field
         */
        String multi_sig_address = randomAlphanumeric(34);
        JSONObject result = new JSONObject();
        result.put("address", multi_sig_address);
        rpc_server.out_result = result;

        /*
         * When createMultiSig is called
         */
        MultiSigAddress multiSig = client.createMultiSigAddress(nrequired, key, key1);
        String actual_multi_sig_address = multiSig.getAddress();

        /*
         * Then the server should have been hit, and with the method "createmultisig"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "createmultisig", rpc_server.in_method);

        /*
         * And the number of required keys should have been sent
         */
        assertEquals("Wrong parameter.", nrequired, ((Number) rpc_server.in_params.get(0)).intValue());

        /*
         * And the keys should have been sent
         */
        JSONArray keys = (JSONArray) rpc_server.in_params.get(1);
        assertEquals("Wrong parameter.", key, ((String) keys.get(0)));
        assertEquals("Wrong parameter.", key1, ((String) keys.get(1)));

        /*
         * And the client method should return the multi_sig_address that the server responded with
         */
        assertEquals("Wrong multi-signature address returned.", multi_sig_address, actual_multi_sig_address);
    }

    @Test
    public void createMultiSigSucceedsWithKeysJustUnderMaximumNumber() throws Exception
    {
        /*
         * Given 20 keys
         */
        String[] ideal_keys = new String[20];
        for (int i = 0; i < 20; i++)
            ideal_keys[i] = randomHex();

        /*
         * And that the validator accepts the input as keys, but rejects them as addresses
         */
        given(validator.isValidAddressFormat(anyString())).willReturn(false);
        given(validator.isValidPublicKeyFormat(anyString())).willReturn(true);

        /*
         * And a number of required addresses to redeem
         */
        int nrequired = 2;

        /*
         * And the server responds with JSON including an address field
         */
        String multi_sig_address = randomAlphanumeric(34);
        JSONObject result = new JSONObject();
        result.put("address", multi_sig_address);
        rpc_server.out_result = result;

        /*
         * When createMultiSig is called
         */
        MultiSigAddress multiSig = client.createMultiSigAddress(nrequired, ideal_keys);
        String actual_multi_sig_address = multiSig.getAddress();

        /*
         * Then the server should have been hit, and with the method "createmultisig"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "createmultisig", rpc_server.in_method);

        /*
         * And the number of required keys should have been sent
         */
        assertEquals("Wrong parameter.", nrequired, ((Number) rpc_server.in_params.get(0)).intValue());

        /*
         * And the keys should have been sent
         */
        JSONArray keys = (JSONArray) rpc_server.in_params.get(1);
        for (int i = 0; i < 20; i++)
            assertEquals("Wrong parameter.", ideal_keys[i], ((String) keys.get(i)));

        /*
         * And the client method should return the multi_sig_address that the server responded with
         */
        assertEquals("Wrong multi-signature address returned.", multi_sig_address, actual_multi_sig_address);
    }

    @Test
    public void createMultiSigSucceedsWithAddresses() throws Exception
    {
        /*
         * Given two keys
         */
        String key = randomHex();
        String key1 = randomHex();

        /*
         * And that the validator accepts the input as addresses, but rejects them as keys
         */
        given(validator.isValidAddressFormat(anyString())).willReturn(true);
        given(validator.isValidPublicKeyFormat(anyString())).willReturn(false);

        /*
         * And a number of required addresses to redeem
         */
        int nrequired = 2;

        /*
         * And the server responds with JSON including an address field
         */
        String multi_sig_address = randomAlphanumeric(34);
        JSONObject result = new JSONObject();
        result.put("address", multi_sig_address);
        rpc_server.out_result = result;

        /*
         * When createMultiSig is called
         */
        MultiSigAddress multiSig = client.createMultiSigAddress(nrequired, key, key1);
        String actual_multi_sig_address = multiSig.getAddress();

        /*
         * Then the server should have been hit, and with the method "createmultisig"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "createmultisig", rpc_server.in_method);

        /*
         * And the number of required keys should have been sent
         */
        assertEquals("Wrong parameter.", nrequired, ((Number) rpc_server.in_params.get(0)).intValue());

        /*
         * And the keys should have been sent
         */
        JSONArray keys = (JSONArray) rpc_server.in_params.get(1);
        assertEquals("Wrong parameter.", key, ((String) keys.get(0)));
        assertEquals("Wrong parameter.", key1, ((String) keys.get(1)));

        /*
         * And the client method should return the multi_sig_address that the server responded with
         */
        assertEquals("Wrong multi-signature address returned.", multi_sig_address, actual_multi_sig_address);
    }

    @Test
    public void createMultiSigThrowsExceptionWhenValidatorRejectsAsKeyAndAsAddress() throws Exception
    {
        /*
         * Given two keys
         */
        String key = randomHex();
        String key1 = randomHex();

        /*
         * And that the validator rejects the input as keys, and also as addresses
         */
        given(validator.isValidAddressFormat(anyString())).willReturn(false);
        given(validator.isValidPublicKeyFormat(anyString())).willReturn(false);

        /*
         * And a number of required addresses to redeem
         */
        int nrequired = 2;

        /*
         * And the server responds with JSON including an address field
         */
        String multi_sig_address = randomAlphanumeric(34);
        JSONObject result = new JSONObject();
        result.put("address", multi_sig_address);
        rpc_server.out_result = result;

        /*
         * When createMultiSig is called
         */
        boolean exception_thrown = false;
        try
        {
            client.createMultiSigAddress(nrequired, key, key1);
        }
        catch (BitcoinException e)
        {
            exception_thrown = true;
        }

        /*
         * Then the server should not have been hit, and with the method "createmultisig"
         */
        assertFalse("Client hit the fake bitcoind RPC server.  It should not have.", rpc_server.was_hit);

        /*
         * And an exception should have been thrown
         */
        assertTrue("Should have thrown an exception.", exception_thrown);
    }

    @Test
    public void createMultiSigThrowsExceptionWhenNumberRequiredOutnumbersKeys() throws Exception
    {
        /*
         * Given two keys
         */
        String key = randomHex();
        String key1 = randomHex();

        /*
         * And that the validator accepts the input as keys, but rejects them as addresses
         */
        given(validator.isValidAddressFormat(anyString())).willReturn(false);
        given(validator.isValidPublicKeyFormat(anyString())).willReturn(true);

        /*
         * And the number of required addresses to redeem is greater than the number of keys
         */
        int nrequired = 3;

        /*
         * And the server responds with JSON including an address field
         */
        String multi_sig_address = randomAlphanumeric(34);
        JSONObject result = new JSONObject();
        result.put("address", multi_sig_address);
        rpc_server.out_result = result;

        /*
         * When createMultiSig is called
         */
        boolean exception_thrown = false;
        try
        {
            client.createMultiSigAddress(nrequired, key, key1);
        }
        catch (BitcoinException e)
        {
            exception_thrown = true;
        }

        /*
         * Then the server should not have been hit, and with the method "createmultisig"
         */
        assertFalse("Client hit the fake bitcoind RPC server.  It should not have.", rpc_server.was_hit);

        /*
         * And an exception should have been thrown
         */
        assertTrue("Should have thrown an exception.", exception_thrown);
    }

    @Test
    public void createMultiSigThrowsExceptionWhenNumberRequiredIsZero() throws Exception
    {
        /*
         * Given two keys
         */
        String key = randomHex();
        String key1 = randomHex();

        /*
         * And that the validator accepts the input as keys, but rejects them as addresses
         */
        given(validator.isValidAddressFormat(anyString())).willReturn(false);
        given(validator.isValidPublicKeyFormat(anyString())).willReturn(true);

        /*
         * And the number of required addresses to redeem is zero
         */
        int nrequired = 0;

        /*
         * And the server responds with JSON including an address field
         */
        String multi_sig_address = randomAlphanumeric(34);
        JSONObject result = new JSONObject();
        result.put("address", multi_sig_address);
        rpc_server.out_result = result;

        /*
         * When createMultiSig is called
         */
        boolean exception_thrown = false;
        try
        {
            client.createMultiSigAddress(nrequired, key, key1);
        }
        catch (BitcoinException e)
        {
            exception_thrown = true;
        }

        /*
         * Then the server should not have been hit, and with the method "createmultisig"
         */
        assertFalse("Client hit the fake bitcoind RPC server.  It should not have.", rpc_server.was_hit);

        /*
         * And an exception should have been thrown
         */
        assertTrue("Should have thrown an exception.", exception_thrown);
    }

    @Test
    public void createMultiSigThrowsExceptionWhenNumberRequiredIsNegative() throws Exception
    {
        /*
         * Given two keys
         */
        String key = randomHex();
        String key1 = randomHex();

        /*
         * And that the validator accepts the input as keys, but rejects them as addresses
         */
        given(validator.isValidAddressFormat(anyString())).willReturn(false);
        given(validator.isValidPublicKeyFormat(anyString())).willReturn(true);

        /*
         * And the number of required addresses to redeem is below zero
         */
        int nrequired = -1;

        /*
         * And the server responds with JSON including an address field
         */
        String multi_sig_address = randomAlphanumeric(34);
        JSONObject result = new JSONObject();
        result.put("address", multi_sig_address);
        rpc_server.out_result = result;

        /*
         * When createMultiSig is called
         */
        boolean exception_thrown = false;
        try
        {
            client.createMultiSigAddress(nrequired, key, key1);
        }
        catch (BitcoinException e)
        {
            exception_thrown = true;
        }

        /*
         * Then the server should not have been hit, and with the method "createmultisig"
         */
        assertFalse("Client hit the fake bitcoind RPC server.  It should not have.", rpc_server.was_hit);

        /*
         * And an exception should have been thrown
         */
        assertTrue("Should have thrown an exception.", exception_thrown);
    }

    @Test
    public void createMultiSigThrowsExceptionWhenNumberOfKeysIsTooBig() throws Exception
    {
        /*
         * Given 21 keys
         */
        String[] keys = new String[21];
        for (int i = 0; i < 21; i++)
            keys[i] = randomHex();

        /*
         * And that the validator accepts the input as keys, but rejects them as addresses
         */
        given(validator.isValidAddressFormat(anyString())).willReturn(false);
        given(validator.isValidPublicKeyFormat(anyString())).willReturn(true);

        /*
         * And the number of required addresses
         */
        int nrequired = 2;

        /*
         * And the server responds with JSON including an address field
         */
        String multi_sig_address = randomAlphanumeric(34);
        JSONObject result = new JSONObject();
        result.put("address", multi_sig_address);
        rpc_server.out_result = result;

        /*
         * When createMultiSig is called
         */
        boolean exception_thrown = false;
        try
        {
            client.createMultiSigAddress(nrequired, keys);
        }
        catch (BitcoinException e)
        {
            exception_thrown = true;
        }

        /*
         * Then the server should not have been hit, and with the method "createmultisig"
         */
        assertFalse("Client hit the fake bitcoind RPC server.  It should not have.", rpc_server.was_hit);

        /*
         * And an exception should have been thrown
         */
        assertTrue("Should have thrown an exception.", exception_thrown);
    }

    @Test
    public void validateAddressSucceeds() throws Exception
    {
        /*
         * Given an address
         */
        String address = randomAlphanumeric(34);

        /*
         * And the server responds with JSON including a pubkey field
         */
        String pubkey = randomAlphanumeric(34);
        JSONObject result = new JSONObject();
        result.put("pubkey", pubkey);
        rpc_server.out_result = result;

        /*
         * When validateAddress is called
         */
        JSONObject address_validation = client.validateAddress(address);

        /*
         * Then the server should have been hit, and with the method "validateaddress"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "validateaddress", rpc_server.in_method);

        /*
         * And the address should have been sent
         */
        assertEquals("Wrong address parameter.", address, ((String) rpc_server.in_params.get(0)));

        /*
         * And the response should contain the expected pubkey value
         */
        assertEquals("Wrong public key value.", pubkey, address_validation.get("pubkey"));
    }

    @Test
    public void decodeRawTransactionSucceeds() throws Exception
    {
        /*
         * Given a raw transaction
         */
        String raw_transaction = randomAlphanumeric(100);

        /*
         * And the server responds with JSON including a pubkey field
         */
        String pubkey = randomAlphanumeric(34);
        JSONObject result = new JSONObject();
        result.put("pubkey", pubkey);
        rpc_server.out_result = result;

        /*
         * When decodeRawTransaction is called
         */
        JSONObject address_validation = client.decodeRawTransaction(raw_transaction);

        /*
         * Then the server should have been hit, and with the method "decoderawtransaction"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "decoderawtransaction", rpc_server.in_method);

        /*
         * And the raw transaction should have been sent
         */
        assertEquals("Wrong parameter.", raw_transaction, ((String) rpc_server.in_params.get(0)));

        /*
         * And the response should contain the expected pubkey value
         */
        assertEquals("Wrong return value. Should return whatever JSON is returned by the server.", pubkey,
                address_validation.get("pubkey"));
    }

    @Test
    public void getRawTransactionSucceeds() throws Exception
    {
        /*
         * Given a txid
         */
        String txid = randomAlphanumeric(30);

        /*
         * And the server responds with a raw transaction
         */
        String raw_transaction = randomAlphanumeric(34);
        rpc_server.out_result = raw_transaction;

        /*
         * When getRawTransaction is called
         */
        String actual_raw_transaction = client.getRawTransaction(txid);

        /*
         * Then the server should have been hit, and with the method "getrawtransaction"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "getrawtransaction", rpc_server.in_method);

        /*
         * And the txid should have been sent
         */
        assertEquals("Wrong parameter.", txid, ((String) rpc_server.in_params.get(0)));

        /*
         * And the response should contain the expected raw transaction
         */
        assertEquals("Wrong return value.", raw_transaction, actual_raw_transaction);
    }

    @Test(expected = NoInformationAvailableAboutTransactionException.class)
    public void getRawTransactionThrowsCorrectExceptionWhenTxOutOfWallet() throws Exception
    {
        /*
         * Given a txid
         */
        String txid = randomAlphanumeric(30);

        /*
         * And the server responds with a No information failure
         */
        JSONObject result = new JSONObject();
        result.put("code", -5);
        result.put("message", "No information available about transaction");
        rpc_server.out_result = result;
        rpc_server.out_return_status = 500;

        /*
         * When getRawTransaction is called
         */
        String actual_raw_transaction = client.getRawTransaction(txid);

        /*
         * Then the server should have been hit, and with the method "getrawtransaction"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "getrawtransaction", rpc_server.in_method);

        /*
         * And the txid should have been sent
         */
        assertEquals("Wrong parameter.", txid, ((String) rpc_server.in_params.get(0)));

        /*
         * And the response should throw an exception...
         */
    }

    @Test(expected = BitcoinException.class)
    public void getRawTransactionThrowsGeneralExceptionGeneralProblem() throws Exception
    {
        /*
         * Given a txid
         */
        String txid = randomAlphanumeric(30);

        /*
         * And the server responds with a failure
         */
        JSONObject result = new JSONObject();
        result.put("code", -1);
        result.put("message", "General problem");
        rpc_server.out_result = result;
        rpc_server.out_return_status = 500;

        /*
         * When getRawTransaction is called
         */
        String actual_raw_transaction = client.getRawTransaction(txid);

        /*
         * Then the server should have been hit, and with the method "getrawtransaction"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "getrawtransaction", rpc_server.in_method);

        /*
         * And the txid should have been sent
         */
        assertEquals("Wrong parameter.", txid, ((String) rpc_server.in_params.get(0)));

        /*
         * And the response should throw an exception...
         */
    }

    @Test
    public void getRawTransactionVerboseSucceeds() throws Exception
    {
        /*
         * Given a txid
         */
        String txid = randomAlphanumeric(30);

        /*
         * And the server responds with a raw transaction
         */
        JSONObject raw_transaction = (JSONObject) JSONValue.parse("{\"time\":1399196276,\"confirmations\":91322,\"hex\":\"0100000001e7f4ae571bc5ce6e7c340483810aaadf483c3ed3fb4f56fe9c7d7e24e53b33de000000000602c701027901ffffffff03801a060000000000116e6b6b930240039d6c76936c940215029cc0ea2101000000001caaaa010016ffffffffffffffffffffffffffffffffffffffffffffa520a1070000000000232102b816848cb82203b8c65153fff1282844e855276f5dbb3df71c67896a2a73bd57ac00000000\",\"locktime\":0,\"vin\":[{\"sequence\":4294967295,\"vout\":0,\"txid\":\"de333be5247e7d9cfe564ffbd33e3c48dfaa0a818304347c6ecec51b57aef4e7\",\"scriptSig\":{\"hex\":\"02c701027901\",\"asm\":\"455 377\"}}],\"vout\":[{\"scriptPubKey\":{\"hex\":\"6e6b6b930240039d6c76936c940215029c\",\"type\":\"nonstandard\",\"asm\":\"OP_2DUP OP_TOALTSTACK OP_TOALTSTACK OP_ADD 832 OP_NUMEQUALVERIFY OP_FROMALTSTACK OP_DUP OP_ADD OP_FROMALTSTACK OP_SUB 533 OP_NUMEQUAL\"},\"n\":0,\"value\":0.0040},{\"scriptPubKey\":{\"hex\":\"aaaa010016ffffffffffffffffffffffffffffffffffffffffffffa5\",\"type\":\"nonstandard\",\"asm\":\"OP_HASH256 OP_HASH256 0 ffffffffffffffffffffffffffffffffffffffffffff OP_WITHIN\"},\"n\":1,\"value\":0.19},{\"scriptPubKey\":{\"hex\":\"2102b816848cb82203b8c65153fff1282844e855276f5dbb3df71c67896a2a73bd57ac\",\"reqSigs\":1,\"addresses\":[\"mvbP1uJzj5RNLG4eutShdSN3BpqysJCvTB\"],\"type\":\"pubkey\",\"asm\":\"02b816848cb82203b8c65153fff1282844e855276f5dbb3df71c67896a2a73bd57 OP_CHECKSIG\"},\"n\":2,\"value\":0.0050}],\"txid\":\"44b9f9ff28fbfc33d4824729b2ec6f2929588faef81717e194fd7fe2a6b5d384\",\"blockhash\":\"000000004644c9af25b0019f1fbb5b6843c54584bdd3cd9bb30b7b2ef819fcad\",\"blocktime\":1399196276,\"version\":1}");
        rpc_server.out_result = raw_transaction;

        /*
         * When getRawTransactionVerbose is called
         */
        JSONObject actual_raw_transaction = client.getRawTransactionVerbose(txid);

        /*
         * Then the server should have been hit, and with the method "getrawtransaction"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "getrawtransaction", rpc_server.in_method);

        /*
         * And the txid should have been sent
         */
        assertEquals("Wrong parameter.", txid, ((String) rpc_server.in_params.get(0)));

        /*
         * And the response should contain the expected raw transaction
         */
        assertEquals("Wrong return value.", raw_transaction, actual_raw_transaction);
    }

    @Test(expected = NoInformationAvailableAboutTransactionException.class)
    public void getRawTransactionVerboseThrowsCorrectExceptionWhenTxOutOfWallet() throws Exception
    {
        /*
         * Given a txid
         */
        String txid = randomAlphanumeric(30);

        /*
         * And the server responds with a No information failure
         */
        JSONObject result = new JSONObject();
        result.put("code", -5);
        result.put("message", "No information available about transaction");
        rpc_server.out_result = result;
        rpc_server.out_return_status = 500;

        /*
         * When getRawTransactionVerbose is called
         */
        client.getRawTransactionVerbose(txid);

        /*
         * Then the server should have been hit, and with the method "getrawtransaction"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "getrawtransaction", rpc_server.in_method);

        /*
         * And the txid should have been sent
         */
        assertEquals("Wrong parameter.", txid, ((String) rpc_server.in_params.get(0)));

        /*
         * And the response should throw an exception...
         */
    }

    @Test(expected = BitcoinException.class)
    public void getRawTransactionVerboseThrowsGeneralExceptionGeneralProblem() throws Exception
    {
        /*
         * Given a txid
         */
        String txid = randomAlphanumeric(30);

        /*
         * And the server responds with a failure
         */
        JSONObject result = new JSONObject();
        result.put("code", -1);
        result.put("message", "General problem");
        rpc_server.out_result = result;
        rpc_server.out_return_status = 500;

        /*
         * When getRawTransactionVerbose is called
         */
        client.getRawTransactionVerbose(txid);

        /*
         * Then the server should have been hit, and with the method "getrawtransaction"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "getrawtransaction", rpc_server.in_method);

        /*
         * And the txid should have been sent
         */
        assertEquals("Wrong parameter.", txid, ((String) rpc_server.in_params.get(0)));

        /*
         * And the response should throw an exception...
         */
    }

    @Test
    public void hasTxIndexReturnsTrue() throws Exception
    {
        /*
         * Given a txid
         */
        String txid = BitcoinD.FOREIGN_TX;

        /*
         * And the server responds with a raw transaction
         */
        String raw_transaction = randomAlphanumeric(34);
        rpc_server.out_result = raw_transaction;

        /*
         * When hasTxIndex is called
         */
        boolean hasTxIndex = client.hasTxIndex();

        /*
         * Then the server should have been hit, and with the method "getrawtransaction"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "getrawtransaction", rpc_server.in_method);

        /*
         * And the txid should have been sent
         */
        assertEquals("Wrong parameter.", txid, ((String) rpc_server.in_params.get(0)));

        /*
         * And the response should be true
         */
        assertTrue("Wrong return value.", hasTxIndex);
    }

    @Test
    public void hasTxIndexReturnsFalse() throws Exception
    {
        /*
         * Given a txid
         */
        String txid = BitcoinD.FOREIGN_TX;

        /*
         * And the server responds with a failure
         */
        JSONObject result = new JSONObject();
        result.put("code", -5);
        result.put("message", "No information available about transaction");
        rpc_server.out_result = result;
        rpc_server.out_return_status = 500;

        /*
         * When hasTxIndex is called
         */
        boolean hasTxIndex = client.hasTxIndex();

        /*
         * Then the server should have been hit, and with the method "getrawtransaction"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "getrawtransaction", rpc_server.in_method);

        /*
         * And the txid should have been sent
         */
        assertEquals("Wrong parameter.", txid, ((String) rpc_server.in_params.get(0)));

        /*
         * And the response should be false
         */
        assertFalse("Wrong return value.", hasTxIndex);
    }

    @Test
    public void createRawTransactionSucceeds() throws Exception
    {
        /*
         * Given enough parameters to specify a transaction
         */
        String input_txid = randomAlphanumeric(30);
        int vout = 347;
        String address = randomAlphanumeric(34);
        String amount = "36671.988196";

        /*
         * And the server responds with a transaction ID
         */
        String txid = randomAlphanumeric(34);
        rpc_server.out_result = txid;

        /*
         * When createRawTransaction is called
         */
        String actual_txid = client.createRawTransaction(input_txid, vout, address, new BigDecimal(amount));

        /*
         * Then the server should have been hit, and with the method "createrawtransaction"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "createrawtransaction", rpc_server.in_method);

        /*
         * And the parameters should have been sent
         */
        JSONArray inputs = (JSONArray) rpc_server.in_params.get(0);
        JSONObject input = (JSONObject) inputs.get(0);
        assertEquals("Wrong value for txid parameter.", input_txid, ((String) input.get("txid")));
        assertEquals("Wrong value for vout parameter.", vout, ((Number) input.get("vout")));

        JSONObject output = (JSONObject) rpc_server.in_params.get(1);
        assertTrue("Wrong value for address parameter.", output.containsKey(address));
        assertEquals("Wrong value for address parameter.", amount, ((Number) output.get(address)).toString());

        /*
         * And the response should contain the expected transaction ID
         */
        assertEquals("Wrong return value.", txid, actual_txid);
    }

    @Test
    public void signRawTransactionSucceeds() throws Exception
    {
        /*
         * Given enough a raw transaction, and a private key
         */
        String raw_transaction = randomAlphanumeric(30);
        String private_key = randomAlphanumeric(34);

        /*
         * And the server responds with a signed raw transaction
         */
        String signed_raw_transaction = randomAlphanumeric(34);
        JSONObject result = new JSONObject();
        result.put("hex", signed_raw_transaction);
        rpc_server.out_result = result;

        /*
         * When signRawTransaction is called
         */
        String actual_signed_raw_transaction =
                client.signRawTransaction(raw_transaction, private_key);

        /*
         * Then the server should have been hit, and with the method "signrawtransaction"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "signrawtransaction", rpc_server.in_method);

        /*
         * And the parameters should have been sent
         */
        assertEquals("Wrong value for raw_transaction parameter.", raw_transaction, ((String) rpc_server.in_params.get(0)));


        JSONArray private_keys = (JSONArray) rpc_server.in_params.get(2);
        assertEquals("Wrong value for private key parameter.", private_key, ((String) private_keys.get(0)));

        /*
         * And the response should contain the expected signed raw transaction
         */
        assertEquals("Wrong return value.", signed_raw_transaction, actual_signed_raw_transaction);
    }

    @Test
    public void signRawTransactionWithInputParametersSucceeds() throws Exception
    {
        /*
         * Given enough parameters to specify a transaction
         */
        String raw_transaction = randomAlphanumeric(30);
        String txid = randomAlphanumeric(30);
        int vout = 347;
        String scriptPubKey = randomAlphanumeric(34);
        String redeemScript = randomAlphanumeric(34);
        String private_key = randomAlphanumeric(34);

        /*
         * And the server responds with a signed raw transaction
         */
        String signed_raw_transaction = randomAlphanumeric(34);
        JSONObject result = new JSONObject();
        result.put("hex", signed_raw_transaction);
        rpc_server.out_result = result;

        /*
         * When signRawTransaction is called
         */
        String actual_signed_raw_transaction =
                client.signRawTransaction(raw_transaction, txid, vout, scriptPubKey, redeemScript, private_key);

        /*
         * Then the server should have been hit, and with the method "signrawtransaction"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "signrawtransaction", rpc_server.in_method);

        /*
         * And the parameters should have been sent
         */
        assertEquals("Wrong value for raw_transaction parameter.", raw_transaction, ((String) rpc_server.in_params.get(0)));


        JSONArray inputs = (JSONArray) rpc_server.in_params.get(1);
        JSONObject input = (JSONObject) inputs.get(0);
        assertEquals("Wrong value for txid parameter.", txid, ((String) input.get("txid")));
        assertEquals("Wrong value for vout parameter.", vout, ((Number) input.get("vout")));
        assertEquals("Wrong value for scriptPubKey parameter.", scriptPubKey, ((String) input.get("scriptPubKey")));
        assertEquals("Wrong value for redeemScript parameter.", redeemScript, ((String) input.get("redeemScript")));


        JSONArray private_keys = (JSONArray) rpc_server.in_params.get(2);
        assertEquals("Wrong value for private key parameter.", private_key, ((String) private_keys.get(0)));

        /*
         * And the response should contain the expected signed raw transaction
         */
        assertEquals("Wrong return value.", signed_raw_transaction, actual_signed_raw_transaction);
    }

    @Test
    public void getScriptPubKeyHexSucceeds() throws Exception
    {
        /*
         * Given a raw transaction
         */
        String raw_transaction = randomAlphanumeric(30);

        /*
         * And the server responds with a decoded transaction
         */
        String scriptPubKeyHex = randomAlphanumeric(30);
        JSONObject result = new JSONObject();
        JSONArray vouts = new JSONArray();
        JSONObject vout = new JSONObject();
        vouts.add(vout);
        JSONObject scriptPubKey = new JSONObject();
        scriptPubKey.put("hex", scriptPubKeyHex);
        vout.put("scriptPubKey", scriptPubKey);
        result.put("vout", vouts);
        rpc_server.out_result = result;

        /*
         * When getScriptPubKeyHex is called
         */
        String actual_scriptPubKeyHex = client.getScriptPubKeyHex(raw_transaction);

        /*
         * Then the server should have been hit, and with the method "decoderawtransaction"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "decoderawtransaction", rpc_server.in_method);

        /*
         * And the raw transaction should have been sent
         */
        assertEquals("Wrong parameter.", raw_transaction, ((String) rpc_server.in_params.get(0)));

        /*
         * And the response should contain the expected transaction ID
         */
        assertEquals("Wrong return value.", scriptPubKeyHex, actual_scriptPubKeyHex);
    }

    @Test
    public void dumpPrivKeySucceeds() throws Exception
    {
        /*
         * Given an address
         */
        String address = randomAlphanumeric(30);

        /*
         * And the server responds with a private key
         */
        String private_key = randomAlphanumeric(34);
        rpc_server.out_result = private_key;

        /*
         * When dumpPrivKey is called
         */
        String actual_private_key = client.dumpPrivKey(address);

        /*
         * Then the server should have been hit, and with the method "dumpprivkey"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "dumpprivkey", rpc_server.in_method);

        /*
         * And the address should have been sent
         */
        assertEquals("Wrong parameter.", address, ((String) rpc_server.in_params.get(0)));

        /*
         * And the response should contain the expected private key
         */
        assertEquals("Wrong return value.", private_key, actual_private_key);
    }

    @Test
    public void sendRawTransactionWithInputParametersSucceeds() throws Exception
    {
        /*
         * Given a raw transaction
         */
        String raw_transaction = randomAlphanumeric(30);

        /*
         * And the server responds with a transaction ID
         */
        String txid = randomAlphanumeric(34);
        rpc_server.out_result = txid;

        /*
         * When sendRawTransaction is called
         */
        String actual_txid = client.sendRawTransaction(raw_transaction);

        /*
         * Then the server should have been hit, and with the method "sendrawtransaction"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "sendrawtransaction", rpc_server.in_method);

        /*
         * And the raw transaction should have been sent
         */
        assertEquals("Wrong parameter.", raw_transaction, ((String) rpc_server.in_params.get(0)));

        /*
         * And the response should contain the expected transaction ID
         */
        assertEquals("Wrong return value.", txid, actual_txid);
    }

    @Test
    public void getNewAddressSucceeds() throws Exception
    {
        /*
         * Given an account name
         */
        String account = randomAlphanumeric(30);

        /*
         * And the server responds with address
         */
        String address = randomAlphanumeric(34);
        rpc_server.out_result = address;

        /*
         * When getNewAddress is called
         */
        String actual_address = client.getNewAddress(account);

        /*
         * Then the server should have been hit, and with the method "getnewaddress"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "getnewaddress", rpc_server.in_method);

        /*
         * And the account name should have been sent
         */
        assertEquals("Wrong parameter.", account, ((String) rpc_server.in_params.get(0)));

        /*
         * And the response should contain the expected address
         */
        assertEquals("Wrong return value.", address, actual_address);
    }

    @Test
    public void getTransactionSucceeds() throws Exception
    {
        /*
         * Given a transaction ID
         */
        String txid = randomAlphanumeric(100);

        /*
         * And the server responds with JSON including a pubkey field
         */
        String pubkey = randomAlphanumeric(34);
        JSONObject result = new JSONObject();
        result.put("pubkey", pubkey);
        rpc_server.out_result = result;

        /*
         * When getTransaction is called
         */
        JSONObject transaction_details = client.getTransaction(txid);

        /*
         * Then the server should have been hit, and with the method "gettransaction"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "gettransaction", rpc_server.in_method);

        /*
         * And the transaction ID should have been sent
         */
        assertEquals("Wrong parameter.", txid, ((String) rpc_server.in_params.get(0)));

        /*
         * And the response should contain the expected pubkey value
         */
        assertEquals("Wrong return value. Should return whatever JSON is returned by the server.", pubkey,
                transaction_details.get("pubkey"));
    }

    @Test
    public void listReceivedByAddressSucceeds() throws Exception
    {
        /*
         * Given that the server responds with JSON objects in an array
         */
        JSONObject result = new JSONObject();
        result.put("name", "zero");

        JSONObject result1 = new JSONObject();
        result.put("name", "one");

        JSONArray array = new JSONArray();
        array.add(result);
        array.add(result1);

        rpc_server.out_result = array;

        /*
         * When listReceivedByAddress is called
         */
        JSONArray received_by_address = client.listReceivedByAddress();

        /*
         * Then the server should have been hit, and with the method "listreceivedbyaddress"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "listreceivedbyaddress", rpc_server.in_method);

        /*
         * And the response should contain what the server sent
         */
        assertEquals("Wrong number items returned", array, received_by_address);
    }

    @Test
    public void addMultiSigAddressSucceeds() throws Exception
    {
        /*
         * Given two keys and an account name
         */
        String key = randomAlphanumeric(34);
        String key1 = randomAlphanumeric(34);
        String account = randomAlphanumeric(34);

        /*
         * And a number of required addresses to redeem
         */
        int nrequired = 72;

        /*
         * And the server responds with address
         */
        String multi_sig_address = randomAlphanumeric(34);
        rpc_server.out_result = multi_sig_address;

        /*
         * When addMultiSigAddress is called
         */
        String actual_multi_sig_address = client.addMultiSigAddress(nrequired, account, key, key1);

        /*
         * Then the server should have been hit, and with the method "addmultisigaddress"
         */
        assertTrue("Client did not successfully hit the fake bitcoind RPC server.  Wrong port?", rpc_server.was_hit);
        assertEquals("Wrong method invoked.", "addmultisigaddress", rpc_server.in_method);

        /*
         * And the number of required addresses should have been sent
         */
        assertEquals("Wrong parameter.", nrequired, ((Number) rpc_server.in_params.get(0)).intValue());

        /*
         * And the keys should have been sent
         */
        JSONArray keys = (JSONArray) rpc_server.in_params.get(1);
        assertEquals("Wrong parameter.", key, ((String) keys.get(0)));
        assertEquals("Wrong parameter.", key1, ((String) keys.get(1)));

        /*
         * And the account should have been sent
         */
        assertEquals("Wrong parameter.", account, ((String) rpc_server.in_params.get(2)));

        /*
         * And the client method should return the multi_sig_address that the server responded with
         */
        assertEquals("Wrong multi-signature address returned.", multi_sig_address, actual_multi_sig_address);
    }

    @Test
    public void getBalanceSucceeds() throws Exception
    {
        /*
         * Given that the server returns a response that is a balance
         */
        Double balance = Double.valueOf("13.80808");
        rpc_server.out_result = balance;

        /*
         * When getPublicKey is called
         */
        BTC actual_balance = client.getBalance();

        /*
         * Then it should return that balance
         */
        assertEquals("Wrong balance returned.", balance.toString(), actual_balance.toBTC());
    }
}


