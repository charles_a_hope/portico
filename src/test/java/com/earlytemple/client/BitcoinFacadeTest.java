/*
 * Copyright 2014 Early Temple
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.earlytemple.client;

import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.EqualsWithDelta;

import java.math.BigDecimal;
import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class BitcoinFacadeTest
{
    private Random random = new Random();
    private BitcoinD bitcoind;
    private BitcoinFacade bitcoinFacade;
    private BitcoinValidator validator;

    @Before
    public void setUp() throws Exception
    {
        bitcoind = mock(BitcoinD.class);
        bitcoinFacade = new BitcoinFacade(bitcoind);

        validator = mock(BitcoinValidator.class);
        bitcoinFacade.validator = validator;
        given(validator.isValidAddressFormat(anyString())).willReturn(true);
        given(validator.isValidPublicKeyFormat(anyString())).willReturn(true);
        given(validator.isValidTransactionFormat(anyString())).willReturn(true);
        given(validator.isValidTransactionIDFormat(anyString())).willReturn(true);
    }

    @Test
    public void createAndAddMultiSigAddress() throws Exception
    {
        /*
         * Given these arguments for createAndAddMultiSigAddress
         */
        int nrequired = random.nextInt();
        String account = randomAlphanumeric(34);
        String key = randomAlphanumeric(34);
        String key1 = randomAlphanumeric(34);
        String key2 = randomAlphanumeric(34);

        /*
         * And BitcoinDFacade.createMultiSigAddress returns this MultiSigAddress
         */
        MultiSigAddress multi_signature_address = new MultiSigAddress(randomAlphanumeric(13), randomAlphanumeric(52));
        given(bitcoind.createMultiSigAddress(nrequired, key, key1, key2)).willReturn(multi_signature_address);

        /*
         * When createAndAddMultiSigAddress is called
         */
        MultiSigAddress actual_multi_signature_address =
                bitcoinFacade.createAndAddMultiSigAddress(nrequired, account, key, key1, key2);

        /*
         * Then BitcoinDFacade.addMultiSigAddress should have been called
         */
        verify(bitcoind).addMultiSigAddress(nrequired, account, key, key1, key2);

        /*
         * And BitcoinDFacade.createMultiSigAddress should have been called
         */
        verify(bitcoind).createMultiSigAddress(nrequired, key, key1, key2);

        /*
         * And the MultiSigAddress from BitcoinDFacade.createMultiSigAddress should have been returned
         */
        assertEquals("Wrong MultiSigAddress was returned.", multi_signature_address, actual_multi_signature_address);
    }

    @Test
    public void sendEarlyTempleServiceFee() throws Exception
    {
        /*
         * Given a SenderPaymentDownload containing a certain et_service_fee, and a certain et_address, and a
         * certain contract_name
         */
        SenderPaymentDownload download = new SenderPaymentDownload();
        double et_service_fee = 32.0000001;
        download.put(SenderPaymentDownload.ET_SERVICE_FEE, et_service_fee);
        String et_address = randomAlphanumeric(34);
        download.put(SenderPaymentDownload.ET_ADDRESS, et_address);
        String contract_name = randomAlphanumeric(12);
        download.put(SenderPaymentDownload.CONTRACT_NAME, contract_name);

        /*
         * And the facade sendToAddress returns this transaction ID
         */
        String txid = randomAlphanumeric(11);
        given(bitcoind.sendToAddress(anyString(), any(BigDecimal.class), anyString(), anyString())).willReturn(txid);

        /*
         * When sendEarlyTempleServiceFee is invoked
         */
        String actual_txid = bitcoinFacade.sendEarlyTempleServiceFee(download);

        /*
         * Then sendEarlyTempleServiceFee should have called BitcoinDFacade.sendToAddress with the expected arguments
         */
        verify(bitcoind).sendToAddress(eq(et_address),
                (BigDecimal) argThat(new EqualsWithDelta(et_service_fee, 0.00000001)), contains(contract_name),
                contains(contract_name));

        /*
         * And sendEarlyTempleServiceFee should have returned the txid returned by bitcoin
         */
        assertEquals("Wrong txid returned.", actual_txid, txid);
    }

    @Test
    public void sendContractedAmountToEscrow() throws Exception
    {
        /*
         * Given a SenderPaymentDownload containing a certain contract_amount, and a certain contract_name
         */
        SenderPaymentDownload download = new SenderPaymentDownload();
        double contract_amount = 23.484;
        download.put(SenderPaymentDownload.CONTRACT_AMOUNT, contract_amount);
        String contract_name = randomAlphanumeric(12);
        download.put(SenderPaymentDownload.CONTRACT_NAME, contract_name);

        /*
         * And a particular contract address
         */
        String contract_address = randomAlphanumeric(34);

        /*
         * And the facade sendToAddress returns this transaction ID
         */
        String txid = randomAlphanumeric(11);
        given(bitcoind.sendToAddress(anyString(), any(BigDecimal.class), anyString(), anyString())).willReturn(txid);

        /*
         * When sendContractedAmountToEscrow is invoked
         */
        String actual_txid = bitcoinFacade.sendContractedAmountToEscrow(download, contract_address);

        /*
         * Then sendContractedAmountToEscrow should have called BitcoinDFacade.sendToAddress with the expected arguments
         */
        verify(bitcoind).sendToAddress(eq(contract_address),
                (BigDecimal) argThat(new EqualsWithDelta(contract_amount, 0.00000001)), contains(contract_name),
                contains(contract_name));

        /*
         * And sendContractedAmountToEscrow should have returned the txid returned by bitcoin
         */
        assertEquals("Wrong txid returned.", actual_txid, txid);
    }

    @Test(expected = BitcoinException.class)
    public void signTransactionThatSpendsFromMultiSigRejectsInvalidSigningAddress() throws Exception
    {
        /*
         * Given that the validator rejects the address input
         */
        given(validator.isValidAddressFormat(anyString())).willReturn(false);

        /*
         * And some arguments
         */
        String signing_address = randomAlphanumeric(34);
        String raw_transaction_that_spends_from_multisig = randomAlphanumeric(34);
        String escrow_load_txid = randomAlphanumeric(34);
        String redeem_script = randomAlphanumeric(34);

        /*
         * When signTransactionThatSpendsFromMultiSig is invoked
         */
        bitcoinFacade.signTransactionThatSpendsFromMultiSig(signing_address, raw_transaction_that_spends_from_multisig,
                escrow_load_txid, redeem_script);

        /*
         * Then a BitcoinException should have been thrown...
         */
    }

    @Test(expected = BitcoinException.class)
    public void signTransactionThatSpendsFromMultiSigRejectsInvalidTransaction() throws Exception
    {
        /*
         * Given that the validator rejects the transaction input
         */
        given(validator.isValidTransactionFormat(anyString())).willReturn(false);

        /*
         * And some arguments
         */
        String signing_address = randomAlphanumeric(34);
        String raw_transaction_that_spends_from_multisig = randomAlphanumeric(34);
        String escrow_load_txid = randomAlphanumeric(34);
        String redeem_script = randomAlphanumeric(34);

        /*
         * When signTransactionThatSpendsFromMultiSig is invoked
         */
        bitcoinFacade.signTransactionThatSpendsFromMultiSig(signing_address, raw_transaction_that_spends_from_multisig,
                escrow_load_txid, redeem_script);

        /*
         * Then a BitcoinException should have been thrown...
         */
    }

    @Test(expected = BitcoinException.class)
    public void signTransactionThatSpendsFromMultiSigRejectsInvalidTransactionID() throws Exception
    {
        /*
         * Given that the validator rejects the transaction ID input
         */
        given(validator.isValidTransactionIDFormat(anyString())).willReturn(false);

        /*
         * And some arguments
         */
        String signing_address = randomAlphanumeric(34);
        String raw_transaction_that_spends_from_multisig = randomAlphanumeric(34);
        String escrow_load_txid = randomAlphanumeric(34);
        String redeem_script = randomAlphanumeric(34);

        /*
         * When signTransactionThatSpendsFromMultiSig is invoked
         */
        bitcoinFacade.signTransactionThatSpendsFromMultiSig(signing_address, raw_transaction_that_spends_from_multisig,
                escrow_load_txid, redeem_script);

        /*
         * Then a BitcoinException should have been thrown...
         */
    }

    @Test
    public void getConfirmationsSucceeds() throws Exception
    {
        JSONObject raw_verbose_transaction = (JSONObject) JSONValue.parse("{\"time\":1399196276,\"confirmations\":91322,\"hex\":\"0100000001e7f4ae571bc5ce6e7c340483810aaadf483c3ed3fb4f56fe9c7d7e24e53b33de000000000602c701027901ffffffff03801a060000000000116e6b6b930240039d6c76936c940215029cc0ea2101000000001caaaa010016ffffffffffffffffffffffffffffffffffffffffffffa520a1070000000000232102b816848cb82203b8c65153fff1282844e855276f5dbb3df71c67896a2a73bd57ac00000000\",\"locktime\":0,\"vin\":[{\"sequence\":4294967295,\"vout\":0,\"txid\":\"de333be5247e7d9cfe564ffbd33e3c48dfaa0a818304347c6ecec51b57aef4e7\",\"scriptSig\":{\"hex\":\"02c701027901\",\"asm\":\"455 377\"}}],\"vout\":[{\"scriptPubKey\":{\"hex\":\"6e6b6b930240039d6c76936c940215029c\",\"type\":\"nonstandard\",\"asm\":\"OP_2DUP OP_TOALTSTACK OP_TOALTSTACK OP_ADD 832 OP_NUMEQUALVERIFY OP_FROMALTSTACK OP_DUP OP_ADD OP_FROMALTSTACK OP_SUB 533 OP_NUMEQUAL\"},\"n\":0,\"value\":0.0040},{\"scriptPubKey\":{\"hex\":\"aaaa010016ffffffffffffffffffffffffffffffffffffffffffffa5\",\"type\":\"nonstandard\",\"asm\":\"OP_HASH256 OP_HASH256 0 ffffffffffffffffffffffffffffffffffffffffffff OP_WITHIN\"},\"n\":1,\"value\":0.19},{\"scriptPubKey\":{\"hex\":\"2102b816848cb82203b8c65153fff1282844e855276f5dbb3df71c67896a2a73bd57ac\",\"reqSigs\":1,\"addresses\":[\"mvbP1uJzj5RNLG4eutShdSN3BpqysJCvTB\"],\"type\":\"pubkey\",\"asm\":\"02b816848cb82203b8c65153fff1282844e855276f5dbb3df71c67896a2a73bd57 OP_CHECKSIG\"},\"n\":2,\"value\":0.0050}],\"txid\":\"44b9f9ff28fbfc33d4824729b2ec6f2929588faef81717e194fd7fe2a6b5d384\",\"blockhash\":\"000000004644c9af25b0019f1fbb5b6843c54584bdd3cd9bb30b7b2ef819fcad\",\"blocktime\":1399196276,\"version\":1}");
        String txid = randomAlphanumeric(11);
        given(bitcoind.getRawTransactionVerbose(txid)).willReturn(raw_verbose_transaction);

        int n_confirmations = bitcoinFacade.getConfirmations(txid);

        assertEquals("getConfirmations() returning wrong number.", 91322, n_confirmations);
    }

    @Test(expected = NoInformationAvailableAboutTransactionException.class)
    public void getConfirmationsThrowsBitcoinExceptionIfTxNotInIndex() throws Exception
    {
        String txid = randomAlphanumeric(11);
        given(bitcoind.getRawTransactionVerbose(txid)).willThrow(NoInformationAvailableAboutTransactionException.class);

        bitcoinFacade.getConfirmations(txid);

        /*
         * Then should throw BitcoinException...
         */
    }

    @Test
    public void getTransaction() throws Exception
    {
        String raw_tx = "{  \n" +
                "   \"time\":1417212659,\n" +
                "   \"confirmations\":9729,\n" +
                "   \"hex\":\"0100000001ac5530bf4213f482e3a2df486a37cce24a8ec862f309807caa3b5e6d6229b437010000006a4730440220600a62a3a0ec177442adb4963a6914f57bd32aeff4d97fe66c353030cad9fb2d02206fed3f0cfc984634972c3c37c62a04b99a6f312d311ec02186d3b57fcb6998d50121029f2a5b66156601e1b60c493a90ab6df8d9dc27525f1f9dcfc827137154852fc1ffffffff02e8aa134f000000001976a914223e7cf24a7ffeeee7f2ab01aff0d75936f7291e88ac50c300000000000017a914a15518b1af893100cfc572da9c9983d1b3a3c6cd8700000000\",\n" +
                "   \"locktime\":0,\n" +
                "   \"vin\":[  \n" +
                "      {  \n" +
                "         \"sequence\":4294967295,\n" +
                "         \"vout\":1,\n" +
                "         \"txid\":\"37b429626d5e3baa7c8009f362c88e4ae2cc376a48dfa2e382f41342bf3055ac\",\n" +
                "         \"scriptSig\":{  \n" +
                "            \"hex\":\"4730440220600a62a3a0ec177442adb4963a6914f57bd32aeff4d97fe66c353030cad9fb2d02206fed3f0cfc984634972c3c37c62a04b99a6f312d311ec02186d3b57fcb6998d50121029f2a5b66156601e1b60c493a90ab6df8d9dc27525f1f9dcfc827137154852fc1\",\n" +
                "            \"asm\":\"30440220600a62a3a0ec177442adb4963a6914f57bd32aeff4d97fe66c353030cad9fb2d02206fed3f0cfc984634972c3c37c62a04b99a6f312d311ec02186d3b57fcb6998d501 029f2a5b66156601e1b60c493a90ab6df8d9dc27525f1f9dcfc827137154852fc1\"\n" +
                "         }\n" +
                "      }\n" +
                "   ],\n" +
                "   \"vout\":[  \n" +
                "      {  \n" +
                "         \"scriptPubKey\":{  \n" +
                "            \"hex\":\"76a914223e7cf24a7ffeeee7f2ab01aff0d75936f7291e88ac\",\n" +
                "            \"reqSigs\":1,\n" +
                "            \"addresses\":[  \n" +
                "               \"mie28KiuWd354abU4Ac66iG9AjUwt5BNuc\"\n" +
                "            ],\n" +
                "            \"type\":\"pubkeyhash\",\n" +
                "            \"asm\":\"OP_DUP OP_HASH160 223e7cf24a7ffeeee7f2ab01aff0d75936f7291e OP_EQUALVERIFY OP_CHECKSIG\"\n" +
                "         },\n" +
                "         \"n\":0,\n" +
                "         \"value\":13.26689\n" +
                "      },\n" +
                "      {  \n" +
                "         \"scriptPubKey\":{  \n" +
                "            \"hex\":\"a914a15518b1af893100cfc572da9c9983d1b3a3c6cd87\",\n" +
                "            \"reqSigs\":1,\n" +
                "            \"addresses\":[  \n" +
                "               \"2N7xGimsSyYDuEQc9ukvB8ZqkmAeUTPq3XN\"\n" +
                "            ],\n" +
                "            \"type\":\"scripthash\",\n" +
                "            \"asm\":\"OP_HASH160 a15518b1af893100cfc572da9c9983d1b3a3c6cd OP_EQUAL\"\n" +
                "         },\n" +
                "         \"n\":1,\n" +
                "         \"value\":5.0E-4\n" +
                "      }\n" +
                "   ],\n" +
                "   \"txid\":\"64d307815e0418278d067ed0da58fa34e7797b4552713ecf3229e35cfff418a2\",\n" +
                "   \"blockhash\":\"00000000000002b24db500bbae32728090a5c697cc420cc30d21224729681b36\",\n" +
                "   \"blocktime\":1417212659,\n" +
                "   \"version\":1\n" +
                "}";
        JSONObject raw_verbose_transaction = (JSONObject) JSONValue.parse(raw_tx);
        String txid = randomAlphanumeric(11);
        given(bitcoind.getRawTransactionVerbose(txid)).willReturn(raw_verbose_transaction);

        Transaction transaction = bitcoinFacade.getTransaction(txid);

        assertEquals("Wrong confirmations.", 9729, transaction.getConfirmations());
        assertEquals("Wrong outputAmountToScriptHash.", new BTC(0.0005), transaction.getOutputAmountToScriptHash());
    }

    @Test
    public void getTransactionWithNoConf() throws Exception
    {
        String raw_tx = "{  \n" +
                "   \"locktime\":0,\n" +
                "   \"txid\":\"d1ab49d86b8beee301e0ed7e516c049557889f104d0c49de707a2dc95af6d68f\",\n" +
                "   \"hex\":\"0100000001692147bf586237135c763a3796ecf3a3c090dbd65bf00d4b775f6f8d084dc4fa000000006a473044022062b94669e078aa3259b2e1ec69fa94b91eee3ffd4a62c356dc73f10c18bf5c6202204e48549d081eb579d9b3ca700b7453f5a5940c38dceff4c978ef9da259ede1570121027dd25282412a8bfbe2f0741885387797291f126e4d88a88e7b7c6c53a6fb4313ffffffff02b8feb34e000000001976a9143811b37c89f09f1fd522d7235fcd33261d2e42a488ac50c300000000000017a914b20e6160c9a6f74b3eac90da5922de225456cef68700000000\",\n" +
                "   \"vin\":[  \n" +
                "      {  \n" +
                "         \"sequence\":4294967295,\n" +
                "         \"scriptSig\":{  \n" +
                "            \"asm\":\"3044022062b94669e078aa3259b2e1ec69fa94b91eee3ffd4a62c356dc73f10c18bf5c6202204e48549d081eb579d9b3ca700b7453f5a5940c38dceff4c978ef9da259ede15701 027dd25282412a8bfbe2f0741885387797291f126e4d88a88e7b7c6c53a6fb4313\",\n" +
                "            \"hex\":\"473044022062b94669e078aa3259b2e1ec69fa94b91eee3ffd4a62c356dc73f10c18bf5c6202204e48549d081eb579d9b3ca700b7453f5a5940c38dceff4c978ef9da259ede1570121027dd25282412a8bfbe2f0741885387797291f126e4d88a88e7b7c6c53a6fb4313\"\n" +
                "         },\n" +
                "         \"txid\":\"fac44d088d6f5f774b0df05bd6db90c0a3f3ec96373a765c13376258bf472169\",\n" +
                "         \"vout\":0\n" +
                "      }\n" +
                "   ],\n" +
                "   \"version\":1,\n" +
                "   \"vout\":[  \n" +
                "      {  \n" +
                "         \"scriptPubKey\":{  \n" +
                "            \"addresses\":[  \n" +
                "               \"mkdRLh5beJpX32RqiZAsB7PmmHUqgwE6px\"\n" +
                "            ],\n" +
                "            \"asm\":\"OP_DUP OP_HASH160 3811b37c89f09f1fd522d7235fcd33261d2e42a4 OP_EQUALVERIFY OP_CHECKSIG\",\n" +
                "            \"hex\":\"76a9143811b37c89f09f1fd522d7235fcd33261d2e42a488ac\",\n" +
                "            \"type\":\"pubkeyhash\",\n" +
                "            \"reqSigs\":1\n" +
                "         },\n" +
                "         \"value\":13.20419,\n" +
                "         \"n\":0\n" +
                "      },\n" +
                "      {  \n" +
                "         \"scriptPubKey\":{  \n" +
                "            \"addresses\":[  \n" +
                "               \"2N9UhVbYDPUJNpTwFS9uMewvgaST4Xevif8\"\n" +
                "            ],\n" +
                "            \"asm\":\"OP_HASH160 b20e6160c9a6f74b3eac90da5922de225456cef6 OP_EQUAL\",\n" +
                "            \"hex\":\"a914b20e6160c9a6f74b3eac90da5922de225456cef687\",\n" +
                "            \"type\":\"scripthash\",\n" +
                "            \"reqSigs\":1\n" +
                "         },\n" +
                "         \"value\":5.0E-4,\n" +
                "         \"n\":1\n" +
                "      }\n" +
                "   ]\n" +
                "}";
        JSONObject raw_verbose_transaction = (JSONObject) JSONValue.parse(raw_tx);
        String txid = randomAlphanumeric(11);
        given(bitcoind.getRawTransactionVerbose(txid)).willReturn(raw_verbose_transaction);

        Transaction transaction = bitcoinFacade.getTransaction(txid);

        assertEquals("Wrong confirmations.", 0, transaction.getConfirmations());
        assertEquals("Wrong outputAmountToScriptHash.", new BTC(0.0005), transaction.getOutputAmountToScriptHash());
    }

    // todo exceptions if 0, >1 p2sh
}
