/*
 * Copyright 2014 Early Temple
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.earlytemple.client;

import org.junit.Test;

import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

public class SenderPaymentDownloadTest
{
    @Test
    public void constructionSucceeds() throws Exception
    {
        /*
         * Given a block of JSON that contains all of the required fields
         */
        String json = "{\n" +
                "\tet_service_fee: 3.2,\n" +
                "\tet_address: \"miA87bgXpRsRQ931WrR1b4G5uCZvi1nQiB\",\n" +
                "\tet_public_key: \"03A68C763B1FB36F4EF634195CF203CF02BF22E7C64A900330E0C66813F7D546A0\",\n" +
                "\tcontract_amount: 1.9,\n" +
                "\tcontract_name: \"A bell is a cup, until it is struck\",\n" +
                "\tcontract_address: \"2N5Dui8CMf4mCef1kCZr8ps1wXmKdT4NEZj\",\n" +
                "\tsender_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\",\n" +
                "\treceiver_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\"\n" +
                "}";

        /*
         * When a new SenderPaymentDownload is constructed
         */
        JSONObject parse = (JSONObject) JSONValue.parse(json);
        new SenderPaymentDownload(parse);

        /*
         * Then it should succeed...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresEarlyTempleServiceFee() throws Exception
    {
        /*
         * Given a block of JSON that is missing the service fee field
         */
        String json = "{\n" +
                "\tet_address: \"miA87bgXpRsRQ931WrR1b4G5uCZvi1nQiB\",\n" +
                "\tet_public_key: \"03A68C763B1FB36F4EF634195CF203CF02BF22E7C64A900330E0C66813F7D546A0\",\n" +
                "\tcontract_amount: 1.9,\n" +
                "\tcontract_name: \"A bell is a cup, until it is struck\",\n" +
                "\tcontract_address: \"2N5Dui8CMf4mCef1kCZr8ps1wXmKdT4NEZj\",\n" +
                "\treceiver_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\"\n" +
                "}";

        /*
         * When a new SenderPaymentDownload is constructed
         */
        JSONObject parse = (JSONObject) JSONValue.parse(json);
        new SenderPaymentDownload(parse);

        /*
         * Then it should throw IllegalArgumentException...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresEarlyTempleServiceFeeAsANumber() throws Exception
    {
        /*
         * Given a block of JSON that has the service fee field as a string
         */
        String json = "{\n" +
                "\tet_service_fee: \"f\",\n" +
                "\tet_address: \"miA87bgXpRsRQ931WrR1b4G5uCZvi1nQiB\",\n" +
                "\tet_public_key: \"03A68C763B1FB36F4EF634195CF203CF02BF22E7C64A900330E0C66813F7D546A0\",\n" +
                "\tcontract_amount: 1.9,\n" +
                "\tcontract_name: \"A bell is a cup, until it is struck\",\n" +
                "\tcontract_address: \"2N5Dui8CMf4mCef1kCZr8ps1wXmKdT4NEZj\",\n" +
                "\tsender_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\",\n" +
                "\treceiver_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\"\n" +
                "\t\n" +
                "}";

        /*
         * When a new SenderPaymentDownload is constructed
         */
        JSONObject parse = (JSONObject) JSONValue.parse(json);
        new SenderPaymentDownload(parse);

        /*
         * Then it should throw IllegalArgumentException...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresContractAmount() throws Exception
    {
        /*
         * Given a block of JSON that is missing the contract amount field
         */
        String json = "{\n" +
                "\tet_service_fee: 3.2,\n" +
                "\tet_address: \"miA87bgXpRsRQ931WrR1b4G5uCZvi1nQiB\",\n" +
                "\tet_public_key: \"03A68C763B1FB36F4EF634195CF203CF02BF22E7C64A900330E0C66813F7D546A0\",\n" +
                "\tcontract_name: \"A bell is a cup, until it is struck\",\n" +
                "\tcontract_address: \"2N5Dui8CMf4mCef1kCZr8ps1wXmKdT4NEZj\",\n" +
                "\treceiver_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\"\n" +
                "\t\n" +
                "}";

        /*
         * When a new SenderPaymentDownload is constructed
         */
        JSONObject parse = (JSONObject) JSONValue.parse(json);
        new SenderPaymentDownload(parse);

        /*
         * Then it should throw IllegalArgumentException...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresContractAmountAsANumber() throws Exception
    {
        /*
         * Given a block of JSON that has the contract amount field as a string
         */
        String json = "{\n" +
                "\tet_service_fee: 3.2,\n" +
                "\tet_address: \"miA87bgXpRsRQ931WrR1b4G5uCZvi1nQiB\",\n" +
                "\tet_public_key: \"03A68C763B1FB36F4EF634195CF203CF02BF22E7C64A900330E0C66813F7D546A0\",\n" +
                "\tcontract_amount: \"finest drops\",\n" +
                "\tcontract_name: \"A bell is a cup, until it is struck\",\n" +
                "\tcontract_address: \"2N5Dui8CMf4mCef1kCZr8ps1wXmKdT4NEZj\",\n" +
                "\tsender_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\",\n" +
                "\treceiver_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\"\n" +
                "\t\n" +
                "}";

        /*
         * When a new SenderPaymentDownload is constructed
         */
        JSONObject parse = (JSONObject) JSONValue.parse(json);
        new SenderPaymentDownload(parse);

        /*
         * Then it should throw IllegalArgumentException...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresContractName() throws Exception
    {
        /*
         * Given a block of JSON that is missing the contract_name field
         */
        String json = "{\n" +
                "\tet_service_fee: 3.2,\n" +
                "\tet_address: \"miA87bgXpRsRQ931WrR1b4G5uCZvi1nQiB\",\n" +
                "\tet_public_key: \"03A68C763B1FB36F4EF634195CF203CF02BF22E7C64A900330E0C66813F7D546A0\",\n" +
                "\tcontract_amount: 1.9,\n" +
                "\tcontract_address: \"2N5Dui8CMf4mCef1kCZr8ps1wXmKdT4NEZj\",\n" +
                "\tsender_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\",\n" +
                "\treceiver_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\"\n" +
                "\t\n" +
                "}";

        /*
         * When a new SenderPaymentDownload is constructed
         */
        JSONObject parse = (JSONObject) JSONValue.parse(json);
        new SenderPaymentDownload(parse);

        /*
         * Then it should throw IllegalArgumentException...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresContractNameAsAString() throws Exception
    {
        /*
         * Given a block of JSON that has the contract_name field as a number
         */
        String json = "{\n" +
                "\tet_service_fee: 3.2,\n" +
                "\tet_address: \"miA87bgXpRsRQ931WrR1b4G5uCZvi1nQiB\",\n" +
                "\tet_public_key: \"03A68C763B1FB36F4EF634195CF203CF02BF22E7C64A900330E0C66813F7D546A0\",\n" +
                "\tcontract_amount: 1.9,\n" +
                "\tcontract_name: 19.5,\n" +
                "\tcontract_address: \"2N5Dui8CMf4mCef1kCZr8ps1wXmKdT4NEZj\",\n" +
                "\tsender_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\",\n" +
                "\treceiver_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\"\n" +
                "\t\n" +
                "}";

        /*
         * When a new SenderPaymentDownload is constructed
         */
        JSONObject parse = (JSONObject) JSONValue.parse(json);
        new SenderPaymentDownload(parse);

        /*
         * Then it should throw IllegalArgumentException...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresEarlyTempleAddress() throws Exception
    {
        /*
         * Given a block of JSON that is missing the et_address field
         */
        String json = "{\n" +
                "\tet_service_fee: 3.2,\n" +
                "\tet_public_key: \"03A68C763B1FB36F4EF634195CF203CF02BF22E7C64A900330E0C66813F7D546A0\",\n" +
                "\tcontract_amount: 1.9,\n" +
                "\tcontract_name: \"A bell is a cup, until it is struck\",\n" +
                "\tcontract_address: \"2N5Dui8CMf4mCef1kCZr8ps1wXmKdT4NEZj\",\n" +
                "\tsender_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\",\n" +
                "\treceiver_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\"\n" +
                "\t\n" +
                "}";

        /*
         * When a new SenderPaymentDownload is constructed
         */
        JSONObject parse = (JSONObject) JSONValue.parse(json);
        new SenderPaymentDownload(parse);

        /*
         * Then it should throw IllegalArgumentException...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresEarlyTempleAddressAsAString() throws Exception
    {
        /*
         * Given a block of JSON that has the et_address field as a number
         */
        String json = "{\n" +
                "\tet_service_fee: 3.2,\n" +
                "\tet_address: 3.2,\n" +
                "\tet_public_key: \"03A68C763B1FB36F4EF634195CF203CF02BF22E7C64A900330E0C66813F7D546A0\",\n" +
                "\tcontract_amount: 1.9,\n" +
                "\tcontract_name: \"A bell is a cup, until it is struck\",\n" +
                "\tcontract_address: \"2N5Dui8CMf4mCef1kCZr8ps1wXmKdT4NEZj\",\n" +
                "\tsender_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\",\n" +
                "\treceiver_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\"\n" +
                "\t\n" +
                "}";

        /*
         * When a new SenderPaymentDownload is constructed
         */
        JSONObject parse = (JSONObject) JSONValue.parse(json);
        new SenderPaymentDownload(parse);

        /*
         * Then it should throw IllegalArgumentException...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresEarlyTemplePubkey() throws Exception
    {
        /*
         * Given a block of JSON that is missing the et_public_key field
         */
        String json = "{\n" +
                "\tet_service_fee: 3.2,\n" +
                "\tet_address: \"miA87bgXpRsRQ931WrR1b4G5uCZvi1nQiB\",\n" +
                "\tcontract_amount: 1.9,\n" +
                "\tcontract_name: \"A bell is a cup, until it is struck\",\n" +
                "\tcontract_address: \"2N5Dui8CMf4mCef1kCZr8ps1wXmKdT4NEZj\",\n" +
                "\tsender_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\",\n" +
                "\treceiver_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\"\n" +
                "\t\n" +
                "}";

        /*
         * When a new SenderPaymentDownload is constructed
         */
        JSONObject parse = (JSONObject) JSONValue.parse(json);
        new SenderPaymentDownload(parse);

        /*
         * Then it should throw IllegalArgumentException...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresEarlyTemplePubkeyAsAString() throws Exception
    {
        /*
         * Given a block of JSON that has the et_public_key field as a number
         */
        String json = "{\n" +
                "\tet_service_fee: 3.2,\n" +
                "\tet_address: \"miA87bgXpRsRQ931WrR1b4G5uCZvi1nQiB\",\n" +
                "\tet_public_key: 5.8,\n" +
                "\tcontract_amount: 1.9,\n" +
                "\tcontract_name: \"A bell is a cup, until it is struck\",\n" +
                "\tcontract_address: \"2N5Dui8CMf4mCef1kCZr8ps1wXmKdT4NEZj\",\n" +
                "\tsender_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\",\n" +
                "\treceiver_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\"\n" +
                "\t\n" +
                "}";

        /*
         * When a new SenderPaymentDownload is constructed
         */
        JSONObject parse = (JSONObject) JSONValue.parse(json);
        new SenderPaymentDownload(parse);

        /*
         * Then it should throw IllegalArgumentException...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresSenderAddress() throws Exception
    {
        /*
         * Given a block of JSON that is missing the sender_address field
         */
        String json = "{\n" +
                "\tet_service_fee: 3.2,\n" +
                "\tet_address: \"miA87bgXpRsRQ931WrR1b4G5uCZvi1nQiB\",\n" +
                "\tet_public_key: \"03A68C763B1FB36F4EF634195CF203CF02BF22E7C64A900330E0C66813F7D546A0\",\n" +
                "\tcontract_amount: 1.9,\n" +
                "\tcontract_name: \"A bell is a cup, until it is struck\",\n" +
                "\tcontract_address: \"2N5Dui8CMf4mCef1kCZr8ps1wXmKdT4NEZj\",\n" +
                "\treceiver_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\"\n" +
                "\t\n" +
                "}";

        /*
         * When a new SenderPaymentDownload is constructed
         */
        JSONObject parse = (JSONObject) JSONValue.parse(json);
        new SenderPaymentDownload(parse);

        /*
         * Then it should throw IllegalArgumentException...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresSenderAddressAsAString() throws Exception
    {
        /*
         * Given a block of JSON that has the sender_address field as a number
         */
        String json = "{\n" +
                "\tet_service_fee: 3.2,\n" +
                "\tet_address: \"miA87bgXpRsRQ931WrR1b4G5uCZvi1nQiB\",\n" +
                "\tet_public_key: \"03A68C763B1FB36F4EF634195CF203CF02BF22E7C64A900330E0C66813F7D546A0\",\n" +
                "\tcontract_amount: 1.9,\n" +
                "\tcontract_name: \"A bell is a cup, until it is struck\",\n" +
                "\tcontract_address: \"2N5Dui8CMf4mCef1kCZr8ps1wXmKdT4NEZj\",\n" +
                "\tsender_address: 19.5,\n" +
                "\treceiver_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\"\n" +
                "\t\n" +
                "}";

        /*
         * When a new SenderPaymentDownload is constructed
         */
        JSONObject parse = (JSONObject) JSONValue.parse(json);
        new SenderPaymentDownload(parse);

        /*
         * Then it should throw IllegalArgumentException...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresReceiverAddress() throws Exception
    {
        /*
         * Given a block of JSON that is missing the receiver_address field
         */
        String json = "{\n" +
                "\tet_service_fee: 3.2,\n" +
                "\tet_address: \"miA87bgXpRsRQ931WrR1b4G5uCZvi1nQiB\",\n" +
                "\tet_public_key: \"03A68C763B1FB36F4EF634195CF203CF02BF22E7C64A900330E0C66813F7D546A0\",\n" +
                "\tcontract_amount: 1.9,\n" +
                "\tcontract_name: \"A bell is a cup, until it is struck\",\n" +
                "\tcontract_address: \"2N5Dui8CMf4mCef1kCZr8ps1wXmKdT4NEZj\",\n" +
                "\tsender_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\",\n" +
                "\t\n" +
                "}";

        /*
         * When a new SenderPaymentDownload is constructed
         */
        JSONObject parse = (JSONObject) JSONValue.parse(json);
        new SenderPaymentDownload(parse);

        /*
         * Then it should throw IllegalArgumentException...
         */
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructionRequiresReceiverAddressAsAString() throws Exception
    {
        /*
         * Given a block of JSON that has the receiver_address field as a number
         */
        String json = "{\n" +
                "\tet_service_fee: 3.2,\n" +
                "\tet_address: \"miA87bgXpRsRQ931WrR1b4G5uCZvi1nQiB\",\n" +
                "\tet_public_key: \"03A68C763B1FB36F4EF634195CF203CF02BF22E7C64A900330E0C66813F7D546A0\",\n" +
                "\tcontract_amount: 1.9,\n" +
                "\tcontract_name: \"A bell is a cup, until it is struck\",\n" +
                "\tcontract_address: \"2N5Dui8CMf4mCef1kCZr8ps1wXmKdT4NEZj\",\n" +
                "\tsender_address: \"mkfSjuegQXa7cPVwoesDXcDennZfRg2iPD\",\n" +
                "\treceiver_address: 19.5\n" +
                "\t\n" +
                "}";

        /*
         * When a new SenderPaymentDownload is constructed
         */
        JSONObject parse = (JSONObject) JSONValue.parse(json);
        new SenderPaymentDownload(parse);

        /*
         * Then it should throw IllegalArgumentException...
         */
    }
}
