/*
 * Copyright 2014 Early Temple
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.earlytemple.client;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 *
 **/
public class BitcoinValidatorTest
{
    private BitcoinValidator validator;

    @Before
    public void setUp() throws Exception
    {
        validator = new BitcoinValidator();
    }

    @Test
    public void isValidAddressFormatReturnsFalseIfContainsSpace() throws Exception
    {
        String address = "mnMJmi8Mdvv4gRwTf VYsZnsxL5rw3N5tR";

        boolean valid = validator.isValidAddressFormat(address);

        assertFalse("isValidAddressFormat should return false if address contains a space.", valid);
    }

    @Test
    public void isValidAddressFormatReturnsFalseIfContainsZero() throws Exception
    {
        String address = "mnMJmi8Mdvv4gRwTfrVYsZnsxL5rw3N5t0";

        boolean valid = validator.isValidAddressFormat(address);

        assertFalse("isValidAddressFormat should return false if address contains the number '0', illegal in Base 58.",
                valid);
    }

    @Test
    public void isValidAddressFormatReturnsFalseIfContainsOscar() throws Exception
    {
        String address = "mnMJmi8Mdvv4gRwTfrVYsZnsxL5rw3N5tO";

        boolean valid = validator.isValidAddressFormat(address);

        assertFalse("isValidAddressFormat should return false if address contains the letter 'O', illegal in Base 58.",
                valid);
    }

    @Test
    public void isValidAddressFormatReturnsFalseIfContainsIndia() throws Exception
    {
        String address = "mnMJmi8Mdvv4gRwTfrVYsZnsxL5rw3N5tI";

        boolean valid = validator.isValidAddressFormat(address);

        assertFalse("isValidAddressFormat should return false if address contains the letter 'I', illegal in Base 58.",
                valid);
    }

    @Test
    public void isValidAddressFormatReturnsFalseIfContainsLima() throws Exception
    {
        String address = "mnMJmi8Mdvv4gRwTfrVYsZnsxL5rw3N5tl";

        boolean valid = validator.isValidAddressFormat(address);

        assertFalse("isValidAddressFormat should return false if address contains the letter 'l', illegal in Base 58.",
                valid);
    }

    @Test
    public void isValidAddressFormatReturnsFalseIfTooShort() throws Exception
    {
        String address = "mnMJmi8Mdvv4gRwTfrVYsZns";
        assertEquals("Failed assumption, test string should be 24 characters.", 24, address.length());

        boolean valid = validator.isValidAddressFormat(address);

        assertFalse("isValidAddressFormat should return false if address is shorter than 25 characters.",
                valid);
    }

    @Test
    public void isValidAddressFormatReturnsFalseIfTooLong() throws Exception
    {
        String address = "mnMJmi8Mdvv4gRwTfrVYsZnsmnMJmi8Mdvv";
        assertEquals("Failed assumption, test string should be 35 characters.", 35, address.length());

        boolean valid = validator.isValidAddressFormat(address);

        assertFalse("isValidAddressFormat should return false if address is longer than 34 characters.",
                valid);
    }

    @Test
    public void isValidAddressFormatReturnsFalseIfContainsControlCharacter() throws Exception
    {
        String address = "mnMJmi8Mdvv4gRwTfrVYsZnsxL5rw3N5t\u0007";

        boolean valid = validator.isValidAddressFormat(address);

        assertFalse("isValidAddressFormat should return false if address contains a control character.",
                valid);
    }

    @Test
    public void isValidAddressFormatReturnsTrue() throws Exception
    {
        String address = "mz2K56HX9PyPAYmqTyBVHDuiCHNJFxRJf8";

        boolean valid = validator.isValidAddressFormat(address);

        assertTrue("isValidAddressFormat should return true if address is valid.", valid);
    }

    @Test
    public void isValidMultisigAddressFormatReturnsFalseIfContainsSpace() throws Exception
    {
        String address = "2My7MtGJFoh8W2pZ4 LmdNYLaCTZu6EXYs5";

        boolean valid = validator.isValidMultisigAddressFormat(address);

        assertFalse("isValidMultisigAddressFormat should return false if address contains a space.", valid);
    }

    @Test
    public void isValidMultisigAddressFormatReturnsFalseIfContainsZero() throws Exception
    {
        String address = "2My7MtGJFoh8W2pZ4PLmdNYLaCTZu6EXYs0";

        boolean valid = validator.isValidMultisigAddressFormat(address);

        assertFalse("isValidMultisigAddressFormat should return false if address contains the number '0', illegal in Base 58.",
                valid);
    }

    @Test
    public void isValidMultisigAddressFormatReturnsFalseIfContainsOscar() throws Exception
    {
        String address = "2My7MtGJFoh8W2pZ4PLmdNYLaCTZu6EXYsO";

        boolean valid = validator.isValidMultisigAddressFormat(address);

        assertFalse("isValidMultisigAddressFormat should return false if address contains the letter 'O', illegal in Base 58.",
                valid);
    }

    @Test
    public void isValidMultisigAddressFormatReturnsFalseIfContainsIndia() throws Exception
    {
        String address = "2My7MtGJFoh8W2pZ4PLmdNYLaCTZu6EXYsI";

        boolean valid = validator.isValidMultisigAddressFormat(address);

        assertFalse("isValidMultisigAddressFormat should return false if address contains the letter 'I', illegal in Base 58.",
                valid);
    }

    @Test
    public void isValidMultisigAddressFormatReturnsFalseIfContainsLima() throws Exception
    {
        String address = "2My7MtGJFoh8W2pZ4PLmdNYLaCTZu6EXYsl";

        boolean valid = validator.isValidMultisigAddressFormat(address);

        assertFalse("isValidMultisigAddressFormat should return false if address contains the letter 'l', illegal in Base 58.",
                valid);
    }

    @Test
    public void isValidMultisigAddressFormatReturnsFalseIfTooShort() throws Exception
    {
        String address = "2nMJmi8Mdvv4gRwTfrVYsZns";
        assertEquals("Failed assumption, test string should be 24 characters.", 24, address.length());

        boolean valid = validator.isValidMultisigAddressFormat(address);

        assertFalse("isValidMultisigAddressFormat should return false if address is shorter than 25 characters.",
                valid);
    }

    @Test
    public void isValidMultisigAddressFormatReturnsFalseIfTooLong() throws Exception
    {
        String address = "2My7MtGJFoh8W2pZ4PLmdNYLaCTZu6EXYs55";
        assertEquals("Failed assumption, test string should be 36 characters.", 36, address.length());

        boolean valid = validator.isValidMultisigAddressFormat(address);

        assertFalse("isValidMultisigAddressFormat should return false if address is longer than 35 characters.",
                valid);
    }

    @Test
    public void isValidMultisigAddressFormatReturnsFalseIfContainsControlCharacter() throws Exception
    {
        String address = "2My7MtGJFoh8W2pZ4PLmdNYLaCTZu6EXYs\u0007";

        boolean valid = validator.isValidMultisigAddressFormat(address);

        assertFalse("isValidMultisigAddressFormat should return false if address contains a control character.",
                valid);
    }

    @Test
    public void isValidMultisigAddressFormatReturnsFalseIfStartingWith1() throws Exception
    {
        String address = "1My7MtGJFoh8W2pZ4PLmdNYLaCTZu6EXYs5";

        boolean valid = validator.isValidMultisigAddressFormat(address);

        assertFalse("isValidMultisigAddressFormat should return false if address starts with 1.", valid);
    }

    @Test
    public void isValidMultisigAddressFormatReturnsFalseIfStartingWith4() throws Exception
    {
        String address = "4My7MtGJFoh8W2pZ4PLmdNYLaCTZu6EXYs5";

        boolean valid = validator.isValidMultisigAddressFormat(address);

        assertFalse("isValidMultisigAddressFormat should return false if address starts with 4.", valid);
    }

    @Test
    public void isValidMultisigAddressFormatReturnsTrueStartingWith2() throws Exception
    {
        String address = "2My7MtGJFoh8W2pZ4PLmdNYLaCTZu6EXYs5";

        boolean valid = validator.isValidMultisigAddressFormat(address);

        assertTrue("isValidMultisigAddressFormat should return true if address is valid.", valid);
    }

    @Test
    public void isValidMultisigAddressFormatReturnsTrueStartingWith3() throws Exception
    {
        String address = "3My7MtGJFoh8W2pZ4PLmdNYLaCTZu6EXYs5";

        boolean valid = validator.isValidMultisigAddressFormat(address);

        assertTrue("isValidMultisigAddressFormat should return true if address is valid.", valid);
    }



    /*
     * Uncompressed key
     * 044f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aa385b6b1b8ead809ca67454d9683fcf2ba03456d6fe2c4abe2b07f0fbdbb2f1c1
     *
     * Compressed key
     * 034f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aa
     */
    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenTooShortForCompressedHavingPrefix02() throws Exception
    {
        String key = "024f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key is too short for a compressed key.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenTooLongForCompressedHavingPrefix02() throws Exception
    {
        String key = "024f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aabb";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key is too long for a compressed key.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenTooShortForCompressedHavingPrefix03() throws Exception
    {
        String key = "034f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key is too short for a compressed key.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenTooLongForCompressedHavingPrefix03() throws Exception
    {
        String key = "034f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aabb";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key is too long for a compressed key.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenTooShortForHavingPrefix04() throws Exception
    {
        String key = "044f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aa385b6b1b8ead809ca67454d968" +
                "3fcf2ba03456d6fe2c4abe2b07f0fbdbb2f1";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key is too short.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenTooLongForHavingPrefix04() throws Exception
    {
        String key = "044f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aa385b6b1b8ead809ca67454d9683" +
                "fcf2ba03456d6fe2c4abe2b07f0fbdbb2f1c1aa";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key is too long.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenContainsNonHexCharacter() throws Exception
    {
        String key = "034f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871ag";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key contains a non-hex character.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenBeginWith01Compressed() throws Exception
    {
        String key = "014f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aa";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key begins with 01.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenBeginWith04Compressed() throws Exception
    {
        String key = "044f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aa";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key begins with 04 for a compressed key.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenBeginWith05Compressed() throws Exception
    {
        String key = "054f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aa";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key begins with 05.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenBeginWith01() throws Exception
    {
        String key = "014f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aa385b6b1b8ead809ca67454d9683" +
                "fcf2ba03456d6fe2c4abe2b07f0fbdbb2f1c1";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key begins with 01 for an uncompressed key.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenBeginWith02() throws Exception
    {
        String key = "024f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aa385b6b1b8ead809ca67454d9683" +
                "fcf2ba03456d6fe2c4abe2b07f0fbdbb2f1c1";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key begins with 02 for an uncompressed key.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenBeginWith03() throws Exception
    {
        String key = "034f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aa385b6b1b8ead809ca67454d9683" +
                "fcf2ba03456d6fe2c4abe2b07f0fbdbb2f1c1";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key begins with 03 for an uncompressed key.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenBeginWith05() throws Exception
    {
        String key = "054f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aa385b6b1b8ead809ca67454d9683" +
                "fcf2ba03456d6fe2c4abe2b07f0fbdbb2f1c1";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key begins with 05.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenContainsSpaceCompressed() throws Exception
    {
        String key = "034f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871  ";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key contains a space character.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenContainsSpace() throws Exception
    {
        String key = "044f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aa385b6b1b8ead809ca67454d9683" +
                "fcf2ba03456d6fe2c4abe2b07f0fbdbb2f1  ";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key contains a space character.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsFalseWhenContainsControlCharacter() throws Exception
    {
        String key = "034f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871a\u0007";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertFalse("isValidPublicKeyFormat should return false if key contains a control character.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsTrueWhenBeginWith02Compressed() throws Exception
    {
        String key = "0280415429a39e03da4403658fdb9a0fc550db041e119d9df46906180fac6897f0";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertTrue("isValidPublicKeyFormat should return true if key is valid for a compressed key.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsTrueWhenBeginWith03Compressed() throws Exception
    {
        String key = "0380415429a39e03da4403658fdb9a0fc550db041e119d9df46906180fac6897f0";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertTrue("isValidPublicKeyFormat should return true if key is valid for a compressed key.", valid);
    }

    @Test
    public void isValidPublicKeyFormatReturnsTrueWhenBeginWith04() throws Exception
    {
        String key = "044f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aa385b6b1b8ead809ca67454d9683" +
                "fcf2ba03456d6fe2c4abe2b07f0fbdbb2f1c1";

        boolean valid = validator.isValidPublicKeyFormat(key);

        assertTrue("isValidPublicKeyFormat should return true if key is valid for an uncompressed key.", valid);
    }


    @Test
    public void isValidTransactionFormatReturnsFalseIfOddNumberOfCharacters() throws Exception
    {
        String tx = "01000000014134c3093bca4b131493c37c3448fccf3948b8efc440bc5304e50f002865debd01000000da004830450" +
                "2203c1548b7cdb0f8de4711a225960bbb2764049a2a9aeda47612392e5d854aabeb022100ddf82f549c3f36464dc9a45b" +
                "3e161f60317ec2efe59fc5d5fc9a1a30ae7fb64a0147304402201fdfefd9366805c15e862a5c907c250c0af9ddccda026" +
                "b463c914e95951c1c2f022051ec7a4f1e8ad75f1085d5510ce9782c29cc46b1bc6fe5828f7fd208c433e2210147522103" +
                "07eed54b7e5de9cfa3aa18ca3f5a3a640a1840a32b7b1ecb7bbbb82569d3ae492103ad2cee2194e725e031c56f5082ace" +
                "ceb6580d08737e6f211a21ca764a0bb181552aeffffffff01a0c44a00000000001976a9148165ca06709cc80d7a59b2c9" +
                "0492e844e8b8189788ac0000000";

        boolean valid = validator.isValidTransactionFormat(tx);

        assertFalse("isValidTransactionFormat should return a false if transaction has an odd number of characters.", valid);
    }

    @Test
    public void isValidTransactionFormatReturnsFalseIfContainsSpace() throws Exception
    {
        String tx = "01000000014134c3093bca4b131493c37c3448fccf3948b8efc440bc5304e50f002865debd01000000da004830450" +
                "2203c1548b7cdb0f8de4711a225960bbb2764049a2a9aeda47612392e5d854aabeb022100ddf82f549c3f36464dc9a45b" +
                "3e161f60317ec2efe59fc5d5fc9a1a30ae7fb64a01473044022 1fdfefd9366805c15e862a5c907c250c0af9ddccda026" +
                "b463c914e95951c1c2f022051ec7a4f1e8ad75f1085d5510ce9782c29cc46b1bc6fe5828f7fd208c433e2210147522103" +
                "07eed54b7e5de9cfa3aa18ca3f5a3a640a1840a32b7b1ecb7bbbb82569d3ae492103ad2cee2194e725e031c56f5082ace" +
                "ceb6580d08737e6f211a21ca764a0bb181552aeffffffff01a0c44a00000000001976a9148165ca06709cc80d7a59b2c9" +
                "0492e844e8b8189788ac0000000";

        boolean valid = validator.isValidTransactionFormat(tx);

        assertFalse("isValidTransactionFormat should return a false if transaction contains a space.", valid);
    }

    @Test
    public void isValidTransactionFormatReturnsFalseIfContainsControlCharacter() throws Exception
    {
        String tx = "01000000014134c3093bca4b131493c37c3448fccf3948b8efc440bc5304e50f002865debd01000000da004830450" +
                "2203c1548b7cdb0f8de4711a225960bbb2764049a2a9aeda47612392e5d854aabeb022100ddf82f549c3f36464dc9a45b" +
                "3e161f60317ec2efe59fc5d5fc9a1a30ae7fb64a01473044022\u00071fdfefd9366805c15e862a5c907c250c0af9ddccda026" +
                "b463c914e95951c1c2f022051ec7a4f1e8ad75f1085d5510ce9782c29cc46b1bc6fe5828f7fd208c433e2210147522103" +
                "07eed54b7e5de9cfa3aa18ca3f5a3a640a1840a32b7b1ecb7bbbb82569d3ae492103ad2cee2194e725e031c56f5082ace" +
                "ceb6580d08737e6f211a21ca764a0bb181552aeffffffff01a0c44a00000000001976a9148165ca06709cc80d7a59b2c9" +
                "0492e844e8b8189788ac0000000";

        boolean valid = validator.isValidTransactionFormat(tx);

        assertFalse("isValidTransactionFormat should return a false if transaction contains a control character.", valid);
    }


    @Test
    public void isValidTransactionFormatReturnsFalseIfTooShort() throws Exception
    {
        String tx = "01000000014134c3093bca4b131493c37c3448fccf3948b8efc440bc5304e50f002865debd01000000da004830450";

        boolean valid = validator.isValidTransactionFormat(tx);

        assertFalse("isValidTransactionFormat should return a false if transaction is too short.", valid);
    }


    @Test
    public void isValidTransactionFormatReturnsTrue() throws Exception
    {
        String tx = "01000000014134c3093bca4b131493c37c3448fccf3948b8efc440bc5304e50f002865debd01000000da004830450" +
                "2203c1548b7cdb0f8de4711a225960bbb2764049a2a9aeda47612392e5d854aabeb022100ddf82f549c3f36464dc9a45b" +
                "3e161f60317ec2efe59fc5d5fc9a1a30ae7fb64a0147304402201fdfefd9366805c15e862a5c907c250c0af9ddccda026" +
                "b463c914e95951c1c2f022051ec7a4f1e8ad75f1085d5510ce9782c29cc46b1bc6fe5828f7fd208c433e2210147522103" +
                "07eed54b7e5de9cfa3aa18ca3f5a3a640a1840a32b7b1ecb7bbbb82569d3ae492103ad2cee2194e725e031c56f5082ace" +
                "ceb6580d08737e6f211a21ca764a0bb181552aeffffffff01a0c44a00000000001976a9148165ca06709cc80d7a59b2c9" +
                "0492e844e8b8189788ac00000000";

        boolean valid = validator.isValidTransactionFormat(tx);

        assertTrue("isValidTransactionFormat should return true if transaction is valid.", valid);
    }

    @Test
    public void isValidTransactionIDFormatReturnsFalseIfOddNumberOfCharacters() throws Exception
    {
        String tx = "6f48ad0fe3190e1e991ff35e1d75a511ed504d13e20b31c02bf4f00d57efe82";

        boolean valid = validator.isValidTransactionIDFormat(tx);

        assertFalse("isValidTransactionIDFormat should return a false if transaction has an odd number of characters.", valid);
    }

    @Test
    public void isValidTransactionIDFormatReturnsFalseIfContainsSpace() throws Exception
    {
        String tx = "6f48ad0fe3190e1e991ff35e1d75a 11ed504d13e20b31c02bf4f00d57efe829";

        boolean valid = validator.isValidTransactionIDFormat(tx);

        assertFalse("isValidTransactionIDFormat should return a false if transaction contains a space.", valid);
    }

    @Test
    public void isValidTransactionIDFormatReturnsFalseIfContainsControlCharacter() throws Exception
    {
        String tx = "6f48ad0fe3190e1e991ff35e1d75a511ed50\u0007d13e20b31c02bf4f00d57efe829";

        boolean valid = validator.isValidTransactionIDFormat(tx);

        assertFalse("isValidTransactionIDFormat should return a false if transaction contains a control character.", valid);
    }


    @Test
    public void isValidTransactionIDFormatReturnsFalseIfTooShort() throws Exception
    {
        String tx = "6f48ad0fe3190e1e991ff35e1d75a511ed504d13e20b31c02bf4f00d57efe8";

        boolean valid = validator.isValidTransactionIDFormat(tx);

        assertFalse("isValidTransactionIDFormat should return a false if transaction is too short.", valid);
    }

    @Test
    public void isValidTransactionIDFormatReturnsFalseIfTooLong() throws Exception
    {
        String tx = "6f48ad0fe3190e1e991ff35e1d75a511ed504d13e20b31c02bf4f00d57efe82900";

        boolean valid = validator.isValidTransactionIDFormat(tx);

        assertFalse("isValidTransactionIDFormat should return a false if transaction is too Long.", valid);
    }


    @Test
    public void isValidTransactionIDFormatReturnsTrue() throws Exception
    {
        String tx = "6f48ad0fe3190e1e991ff35e1d75a511ed504d13e20b31c02bf4f00d57efe829";

        boolean valid = validator.isValidTransactionIDFormat(tx);

        assertTrue("isValidTransactionIDFormat should return true if transaction is valid.", valid);
    }

}
